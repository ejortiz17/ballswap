//
//  ObjectiveMenu.h
//  BallSwap
//
//  Created by Eliud Ortiz on 12/20/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "Level.h"
#import "PosColorObjective.h"
#import "ColorNumObjective.h"
#import "Constants.h"
#import "CommonProtocols.h"
#import "GlobalStuff.h"

@interface ObjectiveMenu : CCNode

@property CCLayoutBox *mainLayoutBox;

-(void)setLevel:(Level*)newLevel;

//creates objective display based on the level
-(void)generateObjectiveDisplay;
-(void)hideSelfAndDisableTouches;
-(void)showSelfAndEnableTouches;

@end
