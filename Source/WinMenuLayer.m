//
//  WinMenuLayer.m
//  BallSwap
//
//  Created by Eliud Ortiz on 1/17/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Gameplay.h"
#import "Level.h"
#import "WinMenuLayer.h"
#import "LevelDataManager.h"

@implementation WinMenuLayer{
    
    CCLabelTTF *_winLabel;
}

-(void)setLevel:(Level *)newLevel{
    _level = newLevel;
}

-(void)setWinLabelString: (NSString*)newString{
    
    _winLabel.string = newString;
}

-(void)toNextLevel{
    
    // if the current level is set, try to access the next level
    if (_level != nil) {
        
        int nextLevelNum = [_level getLevelNum] +1;
        
        LevelDataManager *levelManager = [LevelDataManager new];
        
        BOOL isNextLevelUnlocked = [levelManager getIsUnlockedStatusForLevelNum:nextLevelNum];
        
        // check if nextLevelNum is within the range of available levels
        if (nextLevelNum >= INIT_LEVEL_NUM && nextLevelNum <= NUM_OF_LEVELS && isNextLevelUnlocked) {
            
            CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:nextLevelNum];
            [[CCDirector sharedDirector] replaceScene:newGameplayScene];
        }
        
        // start back at the beginning if we have reached max level numbers
        else if ([_level getLevelNum] == NUM_OF_LEVELS){
            nextLevelNum = 1;
            
            CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:nextLevelNum];
            [[CCDirector sharedDirector] replaceScene:newGameplayScene];
        }
        
        // if not, return to earliest incomplete level
        else {
            
            LevelDataManager *levelManager = [LevelDataManager new];
            
            int leastIncompleteLevel = NUM_OF_LEVELS;
            NSMutableArray *unlockedLevels = [levelManager getUnlockedLevels];
            NSMutableArray *completedLevels = [levelManager getCompletedLevels];
            
            // loop through unlocked levels and compare to completed levels
            for (NSNumber* unlockedWrapper in unlockedLevels) {
                
               BOOL leastIncompleteCandidate = TRUE;
                
                for (NSNumber *completedWrapper in completedLevels) {
                    
                    if ([unlockedWrapper intValue] == [completedWrapper intValue]) {
                        
                        leastIncompleteCandidate = FALSE;
                        break;
                    }
                }
                
                if (leastIncompleteCandidate && [unlockedWrapper intValue]< leastIncompleteLevel) {
                    leastIncompleteLevel = [unlockedWrapper intValue];
                }
            }
            
            
            
            CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:leastIncompleteLevel];
            CCLOG(@"Level \"%i\" could not be loaded. Loading earliest incomplete level (%d)",nextLevelNum,leastIncompleteLevel);
            [[CCDirector sharedDirector] replaceScene:newGameplayScene];
        }
    }
    
    // load initial level if _currentLevel is nil
    else{
        CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:INIT_LEVEL_NUM];
        CCLOG(@"WinMenuLayer _currentLevel is nil. Loading first level instead");
        [[CCDirector sharedDirector] replaceScene:newGameplayScene];
    }
}

-(void)replayLevel{
    
    if (_level != nil) {
        
        int currentLevelNum = [_level getLevelNum];
        CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:currentLevelNum];
        [[CCDirector sharedDirector] replaceScene:newGameplayScene];
    }
    
    // if _currentLevel is nil, load initial level
    else{
        
        CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:INIT_LEVEL_NUM];
        CCLOG(@"WinMenuLayer _currentLevel is nil. Loading first level instead");
        [[CCDirector sharedDirector] replaceScene:newGameplayScene];
    }
}

-(void)toLevelSelect{
    
    CCScene *levelSelectScene = [CCBReader loadAsScene:LEVEL_SELECT_MENU];
    [[CCDirector sharedDirector]replaceScene:levelSelectScene];
}

-(void)toMainScene{
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]replaceScene:mainScene];
}



@end
