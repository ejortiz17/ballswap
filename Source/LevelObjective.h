//
//  LevelObjective.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonProtocols.h"

@interface LevelObjective : NSObject

@property NSMutableDictionary *goalDictionary;


//creates a Progress dictionary from a array of ball objects so that the current state of balls
//can be compared to the levelObjective goalDictionary
+(NSMutableDictionary*)objectiveDictionaryFromBallArray:(NSMutableArray*)arrayOfBallPointers;

//get instanced objective class
-(Class)getObjectiveType;

//compare a dictionary to see if it matches our desired obeective (goal). 
-(BOOL)isObjectiveComplete: (NSMutableArray*)arrayOfBallPointers;

-(void)initGoalDictionary:(NSMutableDictionary *)newDictionary;



@end
