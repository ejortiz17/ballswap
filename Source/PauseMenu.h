//
//  PauseMenu.h
//  BallSwap
//
//  Created by Eliud Ortiz on 1/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@class Gameplay;

@interface PauseMenu : CCNode{
    int _levelNum;
}

// set up and return a Pause Menu with current level int
+(CCScene*)loadPauseMenuSceneWithLevelNum:(int)levelInt;

-(void)setLevelNum:(int)newLevelInt;

-(void)resumeLevel;
-(void)replayLevel;
-(void)toLevelSelect;
-(void)toMainScene;

@end
