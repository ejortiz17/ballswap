//
//  Ball.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/5/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Constants/CommonProtocols.h"
#import "Constants/Constants.h"
#import "CCSprite.h"

@class GlobalStuff;

@interface Ball : CCSprite 

@property ObjectColor objectColor;

//change the ball's color and spriteFrame to match the color
-(void)setColorAndFrame:(ObjectColor)newColor;
-(void)setColorAndFrameWithString:(NSString*)newColorString;

// sets the balls new color with added animation
-(void)setColorWithAnimation:(NSString*)newColorString;

-(NSString*)getColorInStringFormat;

@end
