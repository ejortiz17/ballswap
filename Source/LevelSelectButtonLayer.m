//
//  LevelSelectButtonLayer.m
//  BallSwap
//
//  Created by Eliud Ortiz on 1/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "LevelSelectButtonLayer.h"
#import "Constants.h"

#import "Gameplay.h"
#import "Level.h"
#import "LevelDataManager.h"
#import "SessionDataManager.h"
#import "GameDataCacheManager.h"


@implementation LevelSelectButtonLayer{
    
    CCLayoutBox *_parentLayoutBox;
}

- (void)didLoadFromCCB{
    
    [self setButtonSprites];
    
}


-(void)setButtonSprites{
    
    NSMutableArray *buttonsArray = [self getButtonsArray];
    
    
    if ([[GameDataCacheManager sharedManager] isGameDataSet] == FALSE) {
        
        for (CCButton *button in buttonsArray) {
            
            [self setButtonToLockedSprite:button];
        }
    }
    
    else{
        
        for (CCButton *button in buttonsArray) {
            
            LevelDataManager *levelManager = [LevelDataManager new];
            
            // get integer value from button name
            int buttonInt = [button.name intValue];
            
            if ([levelManager doesLevelDataExistForLevelNum:buttonInt]) {
                
                BOOL isUnlocked = [levelManager getIsUnlockedStatusForLevelNum:buttonInt];
                BOOL isCompleted = [levelManager getIsCompleteStatusForLevelNum:buttonInt];
                BOOL isNew = [levelManager getIsNewStatusForLevelNum:buttonInt];
                
                // if level is locked, set all button's default images to locked frame
                if (isUnlocked == FALSE && isCompleted == FALSE) {
                    
                    [self setButtonToLockedSprite:button];
                }
                
                // if level is unlocked and new, set the font color to yellow
                else if (isUnlocked == TRUE && isCompleted == FALSE && isNew == TRUE){
                    
                    button.label.fontColor = [CCColor colorWithRed:1 green:1 blue:0];
                }
                // if the level is unlocked and complete, set button background to ccompleted sprite
                
                else if (isUnlocked == TRUE && isCompleted == TRUE && isNew == FALSE){
                    
                    [self setButtonToCompletedSprite:button];
                }
            }
            
            // if the level data for that level doesn't exist, the set the lockbutton
            else{
                
                [self setButtonToLockedSprite:button];
            }
        }
        
    }
}

-(void)setButtonToLockedSprite:(CCButton*)button{
    
    //change button label font to black to make more visible against button background
    [button.label setFontColor:[CCColor colorWithRed:.30 green:.30 blue:.30]];
    
    // get string for disabled sprite frame
    NSString *backgroundImgPath = [NSString stringWithFormat:@"%@/%@",
                                   LEVEL_BUTTONS_SPRITE_FOLDER,
                                   LOCKED_LEVEL_SPRITE_STRING];
    
    // change all background images to unlocked if NSUserDefault not set
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgPath]
                            forState:CCControlStateNormal];
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgPath]
                            forState:CCControlStateHighlighted];
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgPath]
                            forState:CCControlStateDisabled];
}

-(void)setButtonToCompletedSprite:(CCButton*)button{
    
    // get string for disabled sprite frame
    NSString *backgroundImgPath = [NSString stringWithFormat:@"%@/%@", LEVEL_BUTTONS_SPRITE_FOLDER, COMPLETED_LEVEL_SPRITE];
    
    NSString *backgroundImgSelectedPath =
    [NSString stringWithFormat:@"%@/%@", LEVEL_BUTTONS_SPRITE_FOLDER,COMPLETED_LEVEL_SPRITE_SELECTED];
    
    // change all background images to unlocked if NSUserDefault not set
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgPath]
                            forState:CCControlStateNormal];
    
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgSelectedPath] forState:CCControlStateHighlighted];
    
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:backgroundImgPath]
                            forState:CCControlStateDisabled];

    
}

-(NSMutableArray*)getButtonsArray{
    
    NSMutableArray *buttonsArray = [NSMutableArray array];
    
    // get all buttons from nested CCLayoutBoxes in CCBReader file
    for (CCLayoutBox *child in [_parentLayoutBox children]) {
        for (CCNode *child2 in [child children]) {
            
            if ([child2 isKindOfClass:[CCButton class]]) {
                [buttonsArray addObject:child2];
            }
        }
    }
    return buttonsArray;
}

-(void)goToButtonLevel:(CCButton*)sender{
    
    CCButton *pressedButton = (CCButton*)sender;
    int levelNum = [pressedButton.name intValue];
    
    LevelDataManager *levelManager = [LevelDataManager new];

    
    NSMutableDictionary *levelDictionary = [levelManager getLevelDataForLevelNum:levelNum];
    
    if (levelDictionary != nil) {
        
        
        BOOL isUnlocked = [levelManager getIsUnlockedStatusForLevelNum:levelNum];
        
        if (isUnlocked == TRUE) {
            @try {
                CCScene *levelScene = [Gameplay loadGameplayWithLevel:levelNum];
                [[CCDirector sharedDirector] replaceScene:levelScene withTransition:[CCTransition transitionCrossFadeWithDuration:.5]];
                
            }
            @catch (NSException *exception) {
                CCLOG(@"Failed to load level %i from LevelSelectButtonLayer", levelNum);
            }
        }
        
        else{
            CCLOG(@"Level is locked.");
        }
    }
    
    else{
        CCLOG(@"Failed to load level %i data.", levelNum);
    }
}



@end
