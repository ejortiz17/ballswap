//
//  ContentNode.h
//  BallSwap
//
//  Created by Eliud Ortiz on 1/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
// Custom class for content node; will have ability to ignore touches in certain cases
#import "CCNode.h"

@interface ContentNode : CCNode

@end
