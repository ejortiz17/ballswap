//
//  Ball.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/5/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Ball.h"

#import "GlobalStuff.h"
#import "CCAnimationCache.h"

@implementation Ball{
}

-(void)didLoadFromCCB{
    
}

-(void)setColorAndFrame:(ObjectColor)newColor{
    
    NSString* newSpriteFrame;
    
    switch (newColor) {
        case kColorBlue:
            newSpriteFrame = [NSString stringWithString:BLUE_BALL_DEFAULT_SPRITE];
            break;
        case kColorGreen:
            newSpriteFrame = [NSString stringWithString:GREEN_BALL_DEFAULT_SPRITE];
            break;
        case kColorOrange:
            newSpriteFrame = [NSString stringWithString:ORANGE_BALL_DEFAULT_SPRITE];
            break;
        case kColorRed:
            newSpriteFrame = [NSString stringWithString:RED_BALL_DEFAULT_SPRITE];
            break;
        case kColorSilver:
            newSpriteFrame = [NSString stringWithString:SILVER_BALL_DEFAULT_SPRITE];
            break;
        case kColorViolet:
            newSpriteFrame = [NSString stringWithString:VIOLET_BALL_DEFAULT_SPRITE];
            break;
        default:
            CCLOG(@"color and spriteframe could not be set -[Ball setColorAndFrame]");
            return;
            
    }
    
    NSString *fullSpritePath = [NSString stringWithFormat:@"%@/%@", BALLS_SPRITE_FOLDER, newSpriteFrame];
    self.spriteFrame = [CCSpriteFrame frameWithImageNamed:fullSpritePath];
    self.objectColor = newColor;
}

-(void)setColorAndFrameWithString:(NSString *)newColorString{
    NSString* newSpriteFrame;
    ObjectColor newColor;
    
    if ([newColorString isEqualToString:BLUE_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:BLUE_BALL_DEFAULT_SPRITE];
        newColor = kColorBlue;
    }
    else if ([newColorString isEqualToString:GREEN_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:GREEN_BALL_DEFAULT_SPRITE];
        newColor = kColorGreen;
    }
    else if ([newColorString isEqualToString:ORANGE_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:ORANGE_BALL_DEFAULT_SPRITE];
        newColor = kColorOrange;
    }
    else if ([newColorString isEqualToString:RED_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:RED_BALL_DEFAULT_SPRITE];
        newColor = kColorRed;
    }
    else if ([newColorString isEqualToString:SILVER_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:SILVER_BALL_DEFAULT_SPRITE];
        newColor = kColorSilver;
    }
    else if ([newColorString isEqualToString:VIOLET_COLOR_STRING]){
        newSpriteFrame = [NSString stringWithString:VIOLET_BALL_DEFAULT_SPRITE];
        newColor = kColorViolet;
    }
    else{
        CCLOG(@"Invalid string for -[Ball setColorAndFrameWithString]");
        return;
    }
    
    NSString *fullSpritePath = [NSString stringWithFormat:@"%@/%@", BALLS_SPRITE_FOLDER, newSpriteFrame];
    self.spriteFrame = [CCSpriteFrame frameWithImageNamed:fullSpritePath];
    self.objectColor = newColor;
    
}

-(void)setColorWithAnimation:(NSString *)newColorString{
    
    
    
    CCLOG(@"Current name of running animation: %@", self.animationManager.runningSequenceName);
    
    // set fadein animation to play immediately after the fadeout
    [self.animationManager setCompletedAnimationCallbackBlock:^(id sender){
        [self playFadeInForColor:[GlobalStuff stringToObjectColor:newColorString]];
        
        [self.animationManager setCompletedAnimationCallbackBlock:^(id sender){
            
        }];
    }];
    // play fadeOut animation of current color...
    [self playFadeOutForColor:self.objectColor];
    
    CCLOG(@"Current name of running animation: %@", self.animationManager.runningSequenceName);

    // set the new color and Frame;
    self.objectColor = [GlobalStuff stringToObjectColor:newColorString];

}


-(NSString*)getColorInStringFormat{
    
    switch (self.objectColor) {
        case kColorBlue:
            return BLUE_COLOR_STRING;
            break;
            
        case kColorGreen:
            return GREEN_COLOR_STRING;
            break;
        
        case kColorOrange:
            return ORANGE_COLOR_STRING;
            break;
            
        case kColorRed:
            return RED_COLOR_STRING;
            break;
            
        case kColorSilver:
            return SILVER_COLOR_STRING;
            
        case kColorViolet:
            return VIOLET_COLOR_STRING;
            
        default:
            return @"NO SET COLOR";
            break;
    }
}

/* ANIMATIONS */


-(void)restoreDefaultTimeline{
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Default Timeline"];
}

-(void)blueFadeOutAnim{
    // the animation manager of each node is stored in the 'animationManager' property
    CCAnimationManager* animationManager = self.animationManager;
    // timelines can be referenced and run by name
    [animationManager runAnimationsForSequenceNamed:@"BlueFadeOut"];
}

-(void)blueFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"BlueFadeIn"];
    
}

-(void)greenFadeOutAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"GreenFadeOut"];
}

-(void)greenFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"GreenFadeIn"];
}

-(void)orangeFadeOutAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"OrangeFadeOut"];
}

-(void)orangeFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"OrangeFadeIn"];
}

-(void)redFadeOutAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"RedFadeOut"];
}

-(void)redFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"RedFadeIn"];
}

-(void)silverFadeOutAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"SilverFadeOut"];
}

-(void)silverFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"SilverFadeIn"];
}

-(void)violetFadeOutAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"VioletFadeOut"];
}

-(void)violetFadeInAnim{
    
    CCAnimationManager *animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"VioletFadeIn"];
}

//
-(void)playFadeOutForColorString:(NSString*)colorString{
    ObjectColor colorToPlay = [GlobalStuff stringToObjectColor:colorString];
    [self playFadeOutForColor:colorToPlay];
}

-(void)playFadeInForColorString:(NSString*)colorString{
    ObjectColor colorToPlay = [GlobalStuff stringToObjectColor:colorString];
    [self playFadeInForColor:colorToPlay];
}

// plays fadeOut animation depending on which color is entered
-(void)playFadeOutForColor:(ObjectColor)colorToPlay{
    
    switch (colorToPlay) {
        case kColorBlue:
            [self performSelector:@selector(blueFadeOutAnim) withObject:nil];
            break;
        case kColorGreen:
            [self performSelector:@selector(greenFadeOutAnim) withObject:nil];
            break;
        case kColorOrange:
            [self performSelector:@selector(orangeFadeOutAnim) withObject:nil];
            break;
        case kColorRed:
            [self performSelector:@selector(redFadeOutAnim) withObject:nil];
            break;
        case kColorSilver:
            [self performSelector:@selector(silverFadeOutAnim) withObject:nil];
            break;
        case kColorViolet:
            [self performSelector:@selector(violetFadeOutAnim) withObject:nil];
            break;
            
        default:
            break;
    }
}

// plays fadeOut animation depending on which color is entered
-(void)playFadeInForColor:(ObjectColor)colorToPlay{
    
    switch (colorToPlay) {
        case kColorBlue:
            [self performSelector:@selector(blueFadeInAnim) withObject:nil];
            break;
        case kColorGreen:
            [self performSelector:@selector(greenFadeInAnim) withObject:nil];
            break;
        case kColorOrange:
            [self performSelector:@selector(orangeFadeInAnim) withObject:nil];
            break;
        case kColorRed:
            [self performSelector:@selector(redFadeInAnim) withObject:nil];
            break;
        case kColorSilver:
            [self performSelector:@selector(silverFadeInAnim) withObject:nil];
            break;
        case kColorViolet:
            [self performSelector:@selector(violetFadeInAnim) withObject:nil];
            break;
            
        default:
            break;
    }
}

@end
