//
//  StoreScene.h
//  BallSwap
//
//  Created by Eliud Ortiz on 5/5/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "CCNode.h"

@interface StoreScene : CCNode  <SKProductsRequestDelegate, SKPaymentTransactionObserver>{
    BOOL areAdsRemoved;
}


- (IBAction)purchase:(SKProduct *)product;
- (IBAction)restore;
- (IBAction)tapsUnlockAllLevels;

@end
