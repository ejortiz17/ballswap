//
//  DrawLine.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#ifndef BallSwap_DrawLine_h
#define BallSwap_DrawLine_h

@protocol DrawLine <NSObject>

-(void)setStrokeLineWithFirstPoint:(CGPoint)point1 andSecondPoint:(CGPoint)point2 inNode:(CCNode*)nodeToDrawOn lineToDraw:(CCDrawNode*)strokeLine;
@end

#endif
