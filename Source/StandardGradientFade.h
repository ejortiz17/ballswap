//
//  StandardGradientFade.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCNodeGradientProtocols.h"


@interface StandardGradientFade : NSObject <ColorChange>
@end
