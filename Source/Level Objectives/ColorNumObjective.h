//
//  ColorNumber.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//
// ColorNumObjective are objectives that pair objectColors to integers. self.goalDicitonary is in the format
// of...
// (NSString*) => (NSNumber*)int

#import "LevelObjective.h"
#import "Ball.h"
#import "Constants.h"
#import "GlobalStuff.h"


@interface ColorNumObjective : LevelObjective

@end
