//
//  ColorNumber.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "ColorNumObjective.h"

@implementation ColorNumObjective

//creates a levelObjective Dictionary in the format of a ColorNum objective [NSString =>NSNumber(int)]
//from an array of ballPointers
+(NSMutableDictionary*)objectiveDictionaryFromBallArray:(NSMutableArray *)arrayOfBallPointers{
    
    NSMutableDictionary* objectiveDictionary = [NSMutableDictionary dictionary];
    
    for (int j = 0; j < [arrayOfBallPointers count]; j++) {
        
        NSValue* currentBallPointer;
        Ball *currentBall;
        
        @try {
            currentBallPointer = [arrayOfBallPointers objectAtIndex:j];
            currentBall = (Ball*)[currentBallPointer pointerValue];
        }
        @catch (NSException *exception) {
            CCLOG(@"[ColorNumObjective objectiveDictionary...] invalid type conversion");
        }
        
        NSString *keyString = [GlobalStuff objectColorToString:currentBall.objectColor];
        
        //if key exists, increase count by one for that key
        if ([objectiveDictionary objectForKey:keyString] != nil) {
            NSNumber *value = [objectiveDictionary objectForKey:keyString];
            NSNumber *newValue = [NSNumber numberWithInt:[value intValue]+1];
            [objectiveDictionary setObject:newValue forKey:keyString];
        }
        //if key doesn't exist, add key and value of one for that key
        else{
            NSNumber *value = [NSNumber numberWithInt:1];
            [objectiveDictionary setObject:value forKey:keyString];
        }
    }
    
    return objectiveDictionary;
}

-(Class)getObjectiveType{
    return [self class];
}

//set goal dictionary according to a color Number objective. i.e. NSString => NSNumber(int)
-(void)initGoalDictionary:(NSMutableDictionary *)newDictionary{
    
    BOOL isNewDictionaryValid = TRUE;
    
    @try {
        for (NSString *keyString in newDictionary) {
            if ([GlobalStuff isValidColorString:keyString]) {
                
                NSNumber *keyValue = [newDictionary objectForKey:keyString];
                int keyValueInt = [keyValue intValue];
                
                //do not include a ColorObject if it has 0 of the color in newDictionary
                if (keyValueInt  < 1) {
                    isNewDictionaryValid = FALSE;
                    break;
                }
            }
            
        }
    }
    @catch (NSException *exception) {
        CCLOG(@"-[ColorNumObjective initGoalDictionary] invalid type conversion");
    }
    
    if (isNewDictionaryValid == TRUE) {
        self.goalDictionary = [NSMutableDictionary dictionaryWithDictionary:newDictionary];
    }
    else{
        CCLOG(@"-[ColorNumObjective initGoalDictionary] failed to initialize");
    }
}

-(BOOL)isObjectiveComplete:(NSMutableArray *)arrayOfBallPointers{
    
    //create an NSMD in same format as ColorNumObjective's goalDictionary, i.e. (NSString*)=>(NSNumber*)int
    NSMutableDictionary* dictionaryInColorNumFormat =
        [ColorNumObjective objectiveDictionaryFromBallArray:arrayOfBallPointers];
    
    if ([dictionaryInColorNumFormat isEqualToDictionary:self.goalDictionary]) {
        return TRUE;
    }
    else{
        return false;
    }
}


@end

