//
//  PosColorObjective.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/7/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "PosColorObjective.h"

@implementation PosColorObjective

+(NSMutableDictionary*)objectiveDictionaryFromBallArray:(NSMutableArray *)arrayOfBallPointers{
    
    NSMutableDictionary *objectiveDictionary = [NSMutableDictionary dictionary];
    
    //loop through all ball pointers and put them in the correct format of PosColor objective
    //i.e., Ball* => NSString* ObjectColor
    for (int j = 0; j < [arrayOfBallPointers count]; j++) {
        
        NSValue *currentBallPointer;
        id currentObject;
        
        @try {
            
            currentBallPointer = [arrayOfBallPointers objectAtIndex:j];
            currentObject = [currentBallPointer pointerValue];
        }
        @catch (NSException *exception) {
            CCLOG(@"[PosColorObjective objectiveDictionary...] invalid type conversion");
        }
        
        if ([currentObject isKindOfClass:[Ball class]]) {
            
            Ball *currentBall = (Ball*)currentObject;
            NSString *currentBallColorString =
            [GlobalStuff objectColorToString:currentBall.objectColor];
            
            BOOL isCurrentBallPointerUnique = TRUE;
            
            //check if currentBallPointer is Unique. If so, add it as a key to objectiveDicitonary...
            for (NSValue *value in objectiveDictionary) {
                if ([currentBallPointer isEqualToValue:value]) {
                    isCurrentBallPointerUnique = FALSE;
                    break;
                }
            }
            
        
            if (isCurrentBallPointerUnique == TRUE) {
                [objectiveDictionary setObject:currentBallColorString forKey:currentBallPointer];
            }
            
            //...if not, change nothing and log the fact that ballPointers contain two same pointers
            else{
                CCLOG(@"[PosColorObj objectiveDictionaryFrom....] there are two identical ball pointers");
            }
        }
    }
    
    return objectiveDictionary;
    
}


-(Class)getObjectiveType{
    return [self class];
}


//check if the input dictionary is in the correct format Ball* => NSString* ObjectColor
-(void)initGoalDictionary:(NSMutableDictionary *)newDictionary{
    
    BOOL isNewDictionaryValid = TRUE;
    
    @try {
        for (NSValue *value in newDictionary) {
            
            id currentObject = [value pointerValue];
            
            if ([currentObject isKindOfClass:[Ball class]]) {
                
                Ball *currentBall = (Ball*)currentObject;
                NSString *currentBallColorString =
                    [GlobalStuff objectColorToString:currentBall.objectColor];
                
                //break out of loop and declare newDictionary FALSE if there is a color that
                //doesn't work
                if ([GlobalStuff isValidColorString:currentBallColorString] == FALSE) {
                    isNewDictionaryValid = FALSE;
                    break;
                }
            }
        }
    }
    @catch (NSException *exception) {
        CCLOG(@"-[PosColorObjective initGoalDictionary] invalid type conversion");
    }
    

    if (isNewDictionaryValid == TRUE) {
        self.goalDictionary = [NSMutableDictionary dictionaryWithDictionary:newDictionary];
    }
    else{
        CCLOG(@"-[ColorNumObjective initGoalDictionary] failed to initialize");
    }
    
    
}

-(BOOL)isObjectiveComplete:(NSMutableArray *)arrayOfBallPointers{
    
    // create an NSMD in same format as PosColorObjective's goalDictionary, i.e.
    // NSValue* (Ball*)=> NSString* ObjectColor
    NSMutableDictionary* dictInPosColorFormat =
    [PosColorObjective objectiveDictionaryFromBallArray:arrayOfBallPointers];
    
    BOOL isGoalMet = TRUE;
    
    // loop through each value in the dictionary created from the arrayOfBallPointers...
    for (NSValue *value0 in dictInPosColorFormat) {
        for (NSValue *value1 in self.goalDictionary) {
            if ([value0 isEqualToValue:value1]) {
                
                // compare the ball in the arrayOfBallPointers to see if it has the same color as
                // the desired value in goalDicitonary
                Ball *ball0 = (Ball*)[value0 pointerValue];
                NSString *ballColorString = [GlobalStuff objectColorToString:ball0.objectColor];
                NSString *goalColorString = [self.goalDictionary objectForKey:value1];
                
                //if the ball in the goal dictionary and the ball in
                if ([ballColorString isEqualToString: goalColorString] == FALSE) {
                    
                    isGoalMet = FALSE;
                    break;
                }
            }
        }
        if (isGoalMet == FALSE) {
            break;
        }
    }
    
    if (isGoalMet == TRUE) {
        return TRUE;
    }
    else{
        return FALSE;
    }
}



@end
