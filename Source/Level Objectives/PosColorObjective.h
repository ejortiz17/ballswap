//
//  PosColorObjective.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/7/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//
// LevelObjective that keeps references to ball pointers and the colors that those balls must be
// *ball => ObjectColor

#import "LevelObjective.h"
#import "Ball.h"
#import "Constants.h"
#import "GlobalStuff.h"


@interface PosColorObjective : LevelObjective

@end
