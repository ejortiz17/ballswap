//
//  LevelSelect.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/29/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface LevelSelect : CCNode

-(void)toMainScene;

@end
