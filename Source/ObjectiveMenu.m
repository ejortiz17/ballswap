//
//  ObjectiveMenu.m
//  BallSwap
//
//  Created by Eliud Ortiz on 12/20/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "ObjectiveMenu.h"

@implementation ObjectiveMenu{
    Level *_level;
    CCNode *_mainLayoutBoxNode;
}

@synthesize mainLayoutBox = _mainLayoutBox;



- (void)didLoadFromCCB {
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    self.visible = YES;
    
    self.mainLayoutBox = [CCLayoutBox node];
    self.mainLayoutBox.anchorPoint = ccp(0.5, 0.5);
    self.mainLayoutBox.direction = CCLayoutBoxDirectionVertical;
    self.mainLayoutBox.spacing = 20.f;
    [self.mainLayoutBox layout];
    [_mainLayoutBoxNode addChild:self.mainLayoutBox];
    self.mainLayoutBox.positionType = CCPositionTypeNormalized;
    self.mainLayoutBox.position = ccp(.5, .5);

    
    [self generateObjectiveDisplay];    
}
-(void)setLevel:(Level*)newLevel{
    _level = newLevel;
}

-(void)generateObjectiveDisplay{
    
    //create display of Color Num format if the objective of the level is ColorNumObjective
    
    if ([_level.levelObjective getObjectiveType] == [ColorNumObjective class]) {
        //display different colors
        
        for (NSString *colorString in _level.levelObjective.goalDictionary) {
            
            // get the sprite ball with proper color and string to display how many
            // balls are required of that color to complete the objective
            ObjectColor objectColor = [GlobalStuff stringToObjectColor:colorString];
            NSNumber *value = [_level.levelObjective.goalDictionary objectForKey:colorString];
            int numberOfBallsOfColorString = [value intValue];
            NSString *intString = [NSString stringWithFormat:@"%d",numberOfBallsOfColorString];
            NSString *ballSpriteString;
            
            switch (objectColor) {
                case kColorBlue:
                    ballSpriteString = BLUE_BALL_DEFAULT_SPRITE;
                    break;
                case kColorGreen:
                    ballSpriteString = GREEN_BALL_DEFAULT_SPRITE;
                    break;
                case kColorOrange:
                    ballSpriteString = ORANGE_BALL_DEFAULT_SPRITE;
                    break;
                case kColorRed:
                    ballSpriteString = RED_BALL_DEFAULT_SPRITE;
                    break;
                case kColorSilver:
                    ballSpriteString = SILVER_BALL_DEFAULT_SPRITE;
                    break;
                case kColorViolet:
                    ballSpriteString = VIOLET_BALL_DEFAULT_SPRITE;
                    break;
            
                default:
                    break;
            }
            
            if (ballSpriteString != nil) {
                
                NSString* fullSpritePath =
                [NSString stringWithFormat:@"%@/%@", BALLS_SPRITE_FOLDER,ballSpriteString];
                
                //create a new layoutBox from the ball sprite and a CCLabelTTF
                CCSprite *ballSprite = [CCSprite spriteWithImageNamed:fullSpritePath];
                CCLabelTTF *numberOfBalls = [CCLabelTTF labelWithString:intString fontName:@"ccbResources/Comfortaa-Bold.ttf" fontSize:18];
                numberOfBalls.fontColor = [CCColor colorWithRed:0 green:0 blue:0]; //Black text color
                
                //position and set up the CCLayoutBox consisting of ballSprite and CCLabelTTF
                CCLayoutBox *layoutToAdd = [CCLayoutBox node];
                layoutToAdd.anchorPoint = ccp(0.5, 0.5);
                layoutToAdd.spacing = 50.f;
                layoutToAdd.direction = CCLayoutBoxDirectionHorizontal;
                [layoutToAdd addChild:ballSprite];
                [layoutToAdd addChild:numberOfBalls];
                
                [self.mainLayoutBox addChild:layoutToAdd];
                [layoutToAdd layout];
                [self.mainLayoutBox needsLayout];
                
            }
        }
        
    }
    
    else if ([_level.levelObjective getObjectiveType] == [PosColorObjective class]){
        
        // loop through each NSVALUE ball pointer and get the desired ball/pointer pairs
        // from levelObjective.goalDicitonary
        
        for (NSValue *ballPointerValue in _level.levelObjective.goalDictionary) {
            
            // get the desired color that a certain ball needs to be to complete objective
            NSString *goalBallColorString =
               [_level.levelObjective.goalDictionary objectForKey:ballPointerValue];
            ObjectColor goalBallColor = [GlobalStuff stringToObjectColor:goalBallColorString];
            NSString *ballSpriteString;
            
            // from the goal ObjectColor, get the desired sprite we want to display
            switch (goalBallColor) {
                case kColorBlue:
                    ballSpriteString = BLUE_BALL_DEFAULT_SPRITE;
                    break;
                case kColorGreen:
                    ballSpriteString = GREEN_BALL_DEFAULT_SPRITE;
                    break;
                case kColorOrange:
                    ballSpriteString = ORANGE_BALL_DEFAULT_SPRITE;
                    break;
                case kColorRed:
                    ballSpriteString = RED_BALL_DEFAULT_SPRITE;
                    break;
                case kColorSilver:
                    ballSpriteString = SILVER_BALL_DEFAULT_SPRITE;
                    break;
                case kColorViolet:
                    ballSpriteString = VIOLET_BALL_DEFAULT_SPRITE;
                    break;
                    
                default:
                    break;
            }
            
            if (ballSpriteString != nil) {
                
                NSString* fullSpritePath =
                [NSString stringWithFormat:@"%@/%@", BALLS_SPRITE_FOLDER,ballSpriteString];
                
                //create a new layoutBox from the ball sprite
                CCSprite *ballSprite = [CCSprite spriteWithImageNamed:fullSpritePath];
                // set ballSprite to percentage-based positioning
                
                ballSprite.positionType = _level.positionType;
                
                // get the actual ball pointer from the NSValue ballPointerValue and use
                // said ball's position to place the desired sprite
                
                Ball *currentBall = (Ball*)[ballPointerValue pointerValue];
                ballSprite.position = currentBall.position;
                
                [_mainLayoutBoxNode addChild:ballSprite];
            }
        }
    }
}

//hide ObjectiveMenu and disableTouches
-(void)hideSelfAndDisableTouches{
    self.visible = FALSE;
    self.userInteractionEnabled = FALSE;
}

// show ObjectiveMenu and enable touches
-(void)showSelfAndEnableTouches{
    self.visible = TRUE;
    self.userInteractionEnabled = TRUE;
}


@end
