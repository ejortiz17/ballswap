//
//  GameDataCacheManager.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameDataCacheManager.h"
#import "SessionDataManager.h"
#import "LevelDataManager.h"
#import "Constants.h"

@implementation GameDataCacheManager{
    
    BOOL _isGameDataSet;
    
}

+(GameDataCacheManager*)sharedManager{
    
    static GameDataCacheManager *_sharedManager = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[GameDataCacheManager alloc] init];
    });
    
    return _sharedManager;
}

-(void)initGameData{
    
    LevelDataManager *levelManager = [LevelDataManager new];
    
    [levelManager createUnlockedLevelsListInCache];
    [levelManager createCompletedLevelsListInCache];
    
    // set levels
    [self initLevelGameData];
    
    // create current session data
    [[SessionDataManager sharedManager] createCurrentLevelSessionDataInCache];
    
    if (![self isKeyValid:NOTIFIED_GAME_COMPLETE]) {
        [self setNotifiedUponGameCompletion:FALSE];
    }
    
    if (![self isKeyValid:UNLOCK_ALL_LEVELS]) {
        [self setUnlockAllLevels:FALSE];
    }
    // create methods that set game data from level 1 thru N
    [self unlockAllLevelsAfterPurchase];
    
    // set bool to see if game data is in fact set
    [self setIsGameDataSet:TRUE];
    
    // preload game sounds
    [self loadGameSounds];
}

-(void)deleteGameData{
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
}

-(void)initLevelGameData{
    
    LevelDataManager *levelManager = [LevelDataManager new];

    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS ; j++) {
        
        [levelManager createLevelInCacheWithLevelNum:j];
        
        //unlock first few levels
        if (j<=INIT_UNLOCKED_LEVELS) {
            [levelManager setIsUnlockedStatus:TRUE forLevelNum:j];
            //[levelManager addLevelToUnlockedLevels:j];
        }
        // set first levels as FREE levels
        if (j <= NUM_OF_FREE_LEVELS) {
            [levelManager setIsFreeStatus:TRUE forLevelNum:j];
        }
    }
}

-(void)setUnlockAllLevels:(BOOL)newBool{
    
    //set BOOL in NSUserDefaults indicating if all levels should be unlocked
    [[NSUserDefaults standardUserDefaults] setBool:newBool forKey:UNLOCK_ALL_LEVELS];
    
}

-(BOOL)unlockAllLevels{
    
    return [[NSUserDefaults standardUserDefaults]boolForKey:UNLOCK_ALL_LEVELS];
}

-(void)unlockAllLevelsAfterPurchase{
    
    if ([self isKeyValid:UNLOCK_ALL_LEVELS] && [self unlockAllLevels]) {
        
        LevelDataManager *levelManager = [LevelDataManager new];
    
        for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS; j++) {
            if (![levelManager getIsUnlockedStatusForLevelNum:j]) {
                [levelManager setIsUnlockedStatus:TRUE forLevelNum:j];
            }
            
        }
    }
    
}

-(void)setIsGameDataSet:(BOOL)newBool{
    // set a bool in NSUserDefaults that standardUserDefaults has indeed been set
    [[NSUserDefaults standardUserDefaults] setBool:newBool forKey:IS_NSUSERDEFAULTS_SET];
}

-(void)setNotifiedUponGameCompletion:(BOOL)newBool{
    [[NSUserDefaults standardUserDefaults]setBool:newBool forKey:NOTIFIED_GAME_COMPLETE];
}

-(BOOL)notifiedUponGameCompletion{
    
    return [[NSUserDefaults standardUserDefaults]boolForKey:NOTIFIED_GAME_COMPLETE];
    
}

-(BOOL)isGameDataSet{
    return [[NSUserDefaults standardUserDefaults]boolForKey:IS_NSUSERDEFAULTS_SET];
}

// check if key is already set in NSUserDefaults
-(BOOL)isKeyValid:(NSString*)stringKey{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:stringKey]!=nil) {
        return TRUE;
    }
    
    return FALSE;
}


-(void)loadGameSounds{
    
    OALSimpleAudio *audioPlayer = [OALSimpleAudio sharedInstance];
    @try {
        [audioPlayer preloadEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, SUCCESS_AUDIO]];
        [audioPlayer preloadEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, SWAP_AUDIO]];
        [audioPlayer preloadEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, NO_SWAP_AUDIO]];
        //[audioPlayer preloadEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, BUTTON_CLICK_AUDIO]];

    }
    @catch (NSException *exception) {
        CCLOG(@"Sound file unable to load from audio folder");
    }
}



@end
