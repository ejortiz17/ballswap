//
//  LevelObjective.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelObjective.h"

@implementation LevelObjective

@synthesize goalDictionary;



-(void)initGoalDictionary:(NSMutableDictionary *)newDictionary{
    
}

-(BOOL)isObjectiveComplete:(NSMutableDictionary *)dictionaryToCheck{
    if ([self.goalDictionary isEqualToDictionary:dictionaryToCheck]) {
        CCLOG(@"Objective Complete");
        return TRUE;
    }
    
    else{
        CCLOG(@"Objective not complete yet");
        return FALSE;
    }
}



@end
