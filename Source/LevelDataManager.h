//
//  LevelDataManager.h
//  BallSwap
//
//  Created by Eliud Ortiz on 2/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
// This class is directly responsible for saving level data

#import <Foundation/Foundation.h>
@class Level;

@interface LevelDataManager : NSObject


-(void)createLevelInCacheWithLevelNum:(int)levelNum;
-(void)createUnlockedLevelsListInCache;
-(void)createCompletedLevelsListInCache;

-(NSMutableDictionary*)getLevelDataForLevelNum:(int)levelNum;
-(void)setLevelData:(NSMutableDictionary*)levelData forLevelNum:(int)levelNum;

-(BOOL)doesLevelDataExistForLevelNum:(int)levelNum;

-(NSString*)getLevelNameForLevelNum:(int)levelNum;
-(int)getStrokeRecordForLevelNum:(int)levelNum;
-(BOOL)getIsUnlockedStatusForLevelNum:(int)levelNum;
-(BOOL)getIsCompleteStatusForLevelNum:(int)levelNum;
-(BOOL)getIsNewStatusForLevelNum:(int)levelNum;
-(BOOL)getIsFreeStatusForLevelNum:(int)levelNum;


-(void)setStrokeRecord:(int)strokeRecord forLevelNum:(int)levelNum;
-(void)setIsUnlockedStatus:(BOOL)newUnlockedStatus forLevelNum:(int)levelNum;
-(void)setIsCompleteStatus:(BOOL)newIsCompleteStatus forLevelNum:(int)levelNum;
-(void)setIsNewStatus:(BOOL)isNewBool forLevelNum:(int)levelNum;
-(void)setIsFreeStatus:(BOOL)isFreeBool forLevelNum:(int)levelNum;

-(NSMutableArray*)getUnlockedLevels;
-(void)setUnlockedLevels:(NSMutableArray*)unlockedLevels;
-(void)addLevelToUnlockedLevels:(int)levelNum;

-(NSMutableArray*)getCompletedLevels;
-(void)setCompletedLevels:(NSMutableArray*)completedLevels;
-(void)addLevelToCompletedLevels:(int)levelNum;

-(int)getMaxUnlockedLevelNum;
-(int)getNextLevelNumTo:(int)levelNum;
-(int)getTotalCompletedLevels;
-(int)getTotalCompletedFreeLevels;
-(int)getTotalUnlockedLevels;
-(int)getTotalUnlockedFreeLevels;
-(NSMutableArray*)getLockedFreeLevels;

-(NSMutableArray*)intersectionUnlockedAndComplete;
-(NSMutableArray*)intersectionUnlockedAndIncomplete;
-(int)minUnlockedAndIncompleteLevel;


-(BOOL)freeLevelsToUnlock;
-(void)unlockNextFreeLevels;
-(void)unlockNextLevels;
-(void)unlockNextLevelAfter:(int)levelNum;

-(void)saveLevelDataforLevelNum:(int)levelNum strokeNum:(int)strokeNum isComplete:(BOOL)completeBool;

@end
