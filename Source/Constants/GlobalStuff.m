//
//  GlobalStuff.m
//  BallSwap
//
//  Created by Eliud Ortiz on 12/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GlobalStuff.h"

@implementation GlobalStuff

+(ObjectColor)stringToObjectColor:(NSString *)colorString{
    if ([colorString isEqualToString:BLUE_COLOR_STRING]) {
        return kColorBlue;
    }
    else if ([colorString isEqualToString:GREEN_COLOR_STRING]){
        return kColorGreen;
    }
    else if ([colorString isEqualToString:ORANGE_COLOR_STRING]){
        return kColorOrange;
    }
    else if ([colorString isEqualToString:RED_COLOR_STRING]){
        return kColorRed;
    }
    else if ([colorString isEqualToString:SILVER_COLOR_STRING]){
        return kColorSilver;
    }
    else if ([colorString isEqualToString:VIOLET_COLOR_STRING]){
        return kColorViolet;
    }
    else{
        CCLOG(@"[GlobalStuff stringToObjectColor]could not convert NSString \"%@\" to ObjectColor",colorString);
        return kColorSilver;
    }
}

+(NSString*)objectColorToString:(ObjectColor)objectColor{
    
    NSString* stringColor;
    
    switch (objectColor) {
        case kColorBlue:
            return BLUE_COLOR_STRING;
            break;
        
        case kColorGreen:
            return GREEN_COLOR_STRING;
            break;
            
        case kColorOrange:
            return ORANGE_COLOR_STRING;
            break;
            
        case kColorRed:
            return RED_COLOR_STRING;
            break;
        
        case kColorSilver:
            return SILVER_COLOR_STRING;
            break;
            
        case kColorViolet:
            return VIOLET_COLOR_STRING;
            break;
            
        default:
            break;
    }
    return stringColor;
}

+(BOOL)isValidColorString:(NSString *)colorString{
    if ([colorString isEqualToString:BLUE_COLOR_STRING]) {
        return TRUE;
    }
    else if ([colorString isEqualToString:GREEN_COLOR_STRING]){
        return TRUE;
    }
    else if ([colorString isEqualToString:ORANGE_COLOR_STRING]){
        return TRUE;
    }
    else if ([colorString isEqualToString:RED_COLOR_STRING]){
        return TRUE;
    }
    else if ([colorString isEqualToString:SILVER_COLOR_STRING]){
        return TRUE;
    }
    else if ([colorString isEqualToString:VIOLET_COLOR_STRING]){
        return TRUE;
    }
    else{
        return FALSE;
    }
}

+(CCColor*)objectColorToCCColor:(ObjectColor)objectColor{
    
    switch (objectColor) {
        case kColorBlue:
            return [CCColor blueColor];
            break;
            
        case kColorGreen:
            return [CCColor greenColor];
            
        case kColorOrange:
            return [CCColor orangeColor];
            
        case kColorRed:
            return [CCColor redColor];
            
        case kColorViolet:
            return [CCColor purpleColor];
            
        default:
            return [CCColor grayColor];
    }
    
}

+(NSString*)getFilePathInDocumentsDir:(NSString *)fileName{
    
    // create list of paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // get documents dir
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // return full path of the fileName appended to fileName
    NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    return path;
}

+(BOOL)doesFilePathExist:(NSString *)filePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:filePath]) {
        return TRUE;
    }
    else{
        return FALSE;
    }
}

+(void)initNSUserDefaultsFromLevelsPlist{
    
    // load levelsDictionary from Levels.plist
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:LEVELS_PLIST_FILENAME ofType:@"plist"];
    NSMutableDictionary *levelsDic = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    // pull data from each key if levelsDic is nonempty
    if ([levelsDic count] != 0) {
        
        @try {
            for (NSString* stringKey in levelsDic) {
                
                //get Level dictionary from string key
                NSMutableDictionary *levelDictionary = [levelsDic objectForKey:stringKey];
                
                // using the original string key from the plist file, store level dictionary in
                // NSUserDefaults.
                // Use BOOL with string key to check if user defaults 
                [[NSUserDefaults standardUserDefaults]setObject:levelDictionary forKey:stringKey];
                [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:IS_NSUSERDEFAULTS_SET];
            }
        }
        @catch (NSException *exception) {
            CCLOG(@"[GlobalStuff initUserDefaults] could not retrieve and save data");
        }
    }
}

+(void)copyFile:(NSString *)filePathToCopy toFilePath:(NSString *)destinationPath{
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager copyItemAtPath:filePathToCopy toPath:destinationPath error:&error];
}

+(NSDictionary*)levelDictionaryFromLevelNumber:(int)levelInt;{
    
    // convert int to string to use for key
    NSString *intStringKey = [NSString stringWithFormat:@"%i",levelInt];
    
    // dictionary from NSUserDefaults
    NSDictionary *levelsDic = [[NSUserDefaults standardUserDefaults]objectForKey:intStringKey];
    
    return levelsDic;
}

+(Level *)levelFromLevelNumber:(int)levelInt{
    NSDictionary *levelDictionary = [self levelDictionaryFromLevelNumber:levelInt];
    NSString *levelName = [levelDictionary objectForKey:LEVEL_NAME_KEY];
    
    NSString *levelPath = [NSString stringWithFormat:@"%@/%@", LEVELS_FOLDER, levelName];
    
    Level *levelToReturn = (Level*)[CCBReader load:levelPath];
    
    return levelToReturn;
    
}

+(CGPoint)normalizeCGPoint:(CGPoint)point{
    
    float newXCoord = point.x/sqrtf(powf(point.x, 2) + powf(point.y, 2));
    float newYCoord = point.y/sqrtf(powf(point.x, 2) + powf(point.y, 2));
    CGPoint normalizedPoint = ccp(newXCoord, newYCoord);
    
    return normalizedPoint;
    
}

@end
