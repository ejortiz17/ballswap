//
//  CommonProtocols.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#ifndef BallSwap_CommonProtocols_h
#define BallSwap_CommonProtocols_h

typedef enum{
    kColorBlue,
    kColorGreen,
    kColorOrange,
    kColorRed,
    kColorSilver,
    kColorViolet,
} ObjectColor;

//defines the side of an arbitrary line an object is on
//used in Gameplay.m in -(void) point:(CGPoint)point0....
typedef enum{
    kFirstSideOfLine,
    kSecondSideOfLine,
    kOnLine,
    
}SideofLine;

typedef enum{
    kFirstColor,
    kSecondColor,
}GradientColorSide;

#endif
