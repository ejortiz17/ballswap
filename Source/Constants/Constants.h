//
//  Constants.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/6/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#ifndef BallSwap_Constants_h
#define BallSwap_Constants_h

extern const int NUM_OF_OBJECT_COLORS;
extern const int NUM_OF_LEVELS;
extern const int NUM_OF_FREE_LEVELS;


// initial level number
extern const int INIT_LEVEL_NUM;

// initial unlocked levels
extern const int INIT_UNLOCKED_LEVELS;

// minimum distance between two points for touch to register
extern const int MIN_DIST_PT1_PT2;

// tutorial slide numbers
extern const int INIT_TUT_SLIDE_INT_VALUE;
extern const int LAST_TUT_SLIDE_INT_VALUE;

// formula values to evaluate next levels to unlock
extern const int NUM_OF_LEVELS_TO_UNLOCK;
extern const int UNLOCKED_TO_COMPLETED_DIFFERENCE;

/* String constants */
extern NSString *const BALLS_SPRITE_FOLDER;
extern NSString *const BLUE_BALL_DEFAULT_SPRITE;
extern NSString *const GREEN_BALL_DEFAULT_SPRITE;
extern NSString *const ORANGE_BALL_DEFAULT_SPRITE;
extern NSString *const RED_BALL_DEFAULT_SPRITE;
extern NSString *const SILVER_BALL_DEFAULT_SPRITE;
extern NSString *const VIOLET_BALL_DEFAULT_SPRITE;

// Audio files
extern NSString *const AUDIO_FOLDER;

extern NSString *const SUCCESS_AUDIO;
extern NSString *const SWAP_AUDIO;
extern NSString *const NO_SWAP_AUDIO;
extern NSString *const BUTTON_CLICK_AUDIO;

//
extern NSString *const BLUE_COLOR_STRING;
extern NSString *const GREEN_COLOR_STRING;
extern NSString *const ORANGE_COLOR_STRING;
extern NSString *const RED_COLOR_STRING;
extern NSString *const SILVER_COLOR_STRING;
extern NSString *const VIOLET_COLOR_STRING;

extern NSString *const LEVEL_BUTTONS_SPRITE_FOLDER;
extern NSString *const LOCKED_LEVEL_SPRITE_STRING;
extern NSString *const COMPLETED_LEVEL_SPRITE;
extern NSString *const COMPLETED_LEVEL_SPRITE_SELECTED;

extern NSString *const LEVELS_FOLDER;

// Levels.plist and NSUserDefaults levelDictionary key constants
extern NSString *const LEVELS_PLIST_FILENAME;
extern NSString *const LEVEL_NAME_KEY;
extern NSString *const LEVEL_NUM_KEY;
extern NSString *const LEVEL_IS_UNLOCKED_KEY;
extern NSString *const LEVEL_IS_COMPLETE_KEY;
extern NSString *const LEVEL_IS_NEW_KEY;
extern NSString *const LEVEL_STROKE_RECORD_KEY;
extern NSString *const LEVEL_IS_FREE_KEY;


extern NSString *const UNLOCKED_LEVELS_KEY;
extern NSString *const COMPLETED_LEVELS_KEY;

// TRUE IF GAME COMPLETE NOTIFICATION HAS ALREADY BEEN SHOWN UPON GAME COMPLETETION
extern NSString *const NOTIFIED_GAME_COMPLETE;

// SessionData Keys
extern NSString *const SESSION_DATA_KEY;
extern NSString *const CURRENT_LEVEL_KEY;
extern NSString *const IS_CURRENT_LEVEL_COMPLETE_KEY;




// animation names
extern NSString *const BLUE_FADE_OUT_ANIM;
extern NSString *const BLUE_FADE_IN_ANIM;
extern NSString *const GREEN_FADE_OUT_ANIM;
extern NSString *const GREEN_FADE_IN_ANIM;
extern NSString *const ORANGE_FADE_OUT_ANIM;
extern NSString *const ORANGE_FADE_IN_ANIM;
extern NSString *const RED_FADE_OUT_ANIM;
extern NSString *const RED_FADE_IN_ANIM;
extern NSString *const SILVER_FADE_OUT_ANIM;
extern NSString *const SILVER_FADE_IN_ANIM;
extern NSString *const VIOLET_FADE_OUT_ANIM;
extern NSString *const VIOLET_FADE_IN_ANIM;

// BOOL to check if NSUserDefaults has been set
extern NSString *const IS_NSUSERDEFAULTS_SET;

// key for boolean to unlock all levels in game
extern NSString *const UNLOCK_ALL_LEVELS;

// Tutorial Layer Path consts
extern NSString *const TUTORIAL_LAYER_PREFIX;
extern NSString *const TUTORIAL_LAYER_FOLDER_PATH;

// CCB files
extern NSString *const LEVEL_SELECT_MENU;







#endif
