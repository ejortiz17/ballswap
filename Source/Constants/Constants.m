//
//  Constants.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/7/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Constants.h"

const int NUM_OF_OBJECT_COLORS = 6;
const int NUM_OF_LEVELS = 100;
const int NUM_OF_FREE_LEVELS = 50;



const int INIT_LEVEL_NUM = 1;
const int INIT_UNLOCKED_LEVELS = 10;

const int MIN_DIST_PT1_PT2 = 75;

const int INIT_TUT_SLIDE_INT_VALUE = 0;
const int LAST_TUT_SLIDE_INT_VALUE = 7;

const int NUM_OF_LEVELS_TO_UNLOCK = 5;
const int UNLOCKED_TO_COMPLETED_DIFFERENCE = 5;

// ball sprite folder and files
NSString *const BALLS_SPRITE_FOLDER = @"Balls";
NSString *const BLUE_BALL_DEFAULT_SPRITE = @"bOrb.png";
NSString *const GREEN_BALL_DEFAULT_SPRITE = @"gOrb.png";
NSString *const ORANGE_BALL_DEFAULT_SPRITE = @"oOrb.png";
NSString *const RED_BALL_DEFAULT_SPRITE = @"rOrb.png";
NSString *const SILVER_BALL_DEFAULT_SPRITE = @"slvOrb.png";
NSString *const VIOLET_BALL_DEFAULT_SPRITE = @"vOrb.png";

NSString *const BLUE_COLOR_STRING = @"BLUE";
NSString *const GREEN_COLOR_STRING = @"GREEN";
NSString *const ORANGE_COLOR_STRING = @"ORANGE";
NSString *const RED_COLOR_STRING = @"RED";
NSString *const SILVER_COLOR_STRING = @"SILVER";
NSString *const VIOLET_COLOR_STRING = @"VIOLET";

NSString *const LEVEL_BUTTONS_SPRITE_FOLDER = @"LevelButtons";
NSString *const LOCKED_LEVEL_SPRITE_STRING = @"crBlock.png";
NSString *const COMPLETED_LEVEL_SPRITE = @"yRing.png";
NSString *const COMPLETED_LEVEL_SPRITE_SELECTED= @"yRing_selected_10.png";

// levels folder
NSString *const LEVELS_FOLDER = @"Levels";

// audio files
NSString *const AUDIO_FOLDER = @"Audio";
NSString *const SUCCESS_AUDIO = @"success2.mp3";
NSString *const SWAP_AUDIO = @"swap2.mp3";
NSString *const NO_SWAP_AUDIO = @"noswap2.mp3";
NSString *const BUTTON_CLICK_AUDIO = @"buttonclick.mp3";

NSString *const LEVELS_PLIST_FILENAME = @"Levels";
NSString *const LEVEL_NAME_KEY = @"levelName";
NSString *const LEVEL_NUM_KEY = @"levelNumber";
NSString *const LEVEL_IS_UNLOCKED_KEY = @"isUnlocked";
NSString *const LEVEL_IS_COMPLETE_KEY = @"isComplete";
NSString *const LEVEL_IS_NEW_KEY = @"isNew";
NSString *const LEVEL_IS_FREE_KEY = @"isFree";


NSString *const LEVEL_STROKE_RECORD_KEY = @"strokeRecord";
NSString *const UNLOCKED_LEVELS_KEY = @"unlockedLevels";
NSString *const COMPLETED_LEVELS_KEY = @"completedLevels";
NSString *const NOTIFIED_GAME_COMPLETE = @"notifiedGameComplete";

NSString *const SESSION_DATA_KEY = @"sessionData";
NSString *const CURRENT_LEVEL_KEY = @"currentLevel";
NSString *const IS_CURRENT_LEVEL_COMPLETE_KEY = @"isCurrentLevelComplete";

NSString *const BLUE_FADE_OUT_ANIM = @"BlueFadeOut";
NSString *const BLUE_FADE_IN_ANIM = @"BlueFadeIn";
NSString *const GREEN_FADE_OUT_ANIM = @"GreenFadeOut";
NSString *const GREEN_FADE_IN_ANIM = @"GreenFadeIn";
NSString *const ORANGE_FADE_OUT_ANIM = @"OrangeFadeOut";
NSString *const ORANGE_FADE_IN_ANIM = @"OrangeFadeIn";
NSString *const RED_FADE_OUT_ANIM = @"RedFadeOut";
NSString *const RED_FADE_IN_ANIM = @"RedFadeIn";
NSString *const SILVER_FADE_OUT_ANIM = @"SilverFadeOut";
NSString *const SILVER_FADE_IN_ANIM = @"SilverFadeIn";
NSString *const VIOLET_FADE_OUT_ANIM = @"VioletFadeOut";
NSString *const VIOLET_FADE_IN_ANIM = @"VioletFadeIn";







NSString *const IS_NSUSERDEFAULTS_SET = @"isNSUserDefaultsSet";
NSString *const UNLOCK_ALL_LEVELS = @"unlockAllLevels";


NSString *const TUTORIAL_LAYER_PREFIX = @"TutorialLayer_";
NSString *const TUTORIAL_LAYER_FOLDER_PATH = @"TutorialLayers";

// CCB Files

NSString *const LEVEL_SELECT_MENU = @"LevelSelect";
