//
//  GlobalStuff.h
//  BallSwap
//
//  Created by Eliud Ortiz on 12/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonProtocols.h"
#import "Constants.h"
#import "Level.h"

@interface GlobalStuff : NSObject

+(ObjectColor)stringToObjectColor:(NSString*)colorString;
+(NSString*)objectColorToString:(ObjectColor)objectColor;
+(BOOL)isValidColorString:(NSString*)colorString;
+(CCColor*)objectColorToCCColor:(ObjectColor)objectColor;

// gets a specific level's info from Levels plist from the corresponding level number
+(NSDictionary*)levelDictionaryFromLevelNumber:(int)levelInt;
+(Level*)levelFromLevelNumber:(int)levelInt;

// returns the file name as if it were located in the documents directory
+(NSString*)getFilePathInDocumentsDir:(NSString*)fileName;

// checks if the filePath already exists
+(BOOL)doesFilePathExist:(NSString*)filePath;

// copy file to given destination path
+(void)copyFile:(NSString*)filePathToCopy toFilePath:(NSString*)destinationPath;

// initialize NSUserDefaults from the created Levels plist
// sets a bool TRUE for key @"initFromLevelsPlist" on NSUserDefaults 
+(void)initNSUserDefaultsFromLevelsPlist;

+(CGPoint)normalizeCGPoint:(CGPoint)point;


@end
