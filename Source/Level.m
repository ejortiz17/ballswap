//
//  Level.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Level.h"

#import "LevelDataManager.h"
#import "SessionDataManager.h"
#import "GlobalStuff.h"
#import "StandardColorSwap.h"
#import "StandardPlaneDivision.h"
#import "SimpleLineDraw.h"
#import "BallSwapBool.h"

@implementation Level{
    
    BOOL _objectiveComplete;
    
    CGPoint _firstTouchPoint;
    CGPoint _secondTouchPoint;
    BOOL _secondTouchPointSet;
    
    StandardColorSwap *_colorSwapObj;
    StandardPlaneDivision *_planeDivisionObj;
    SimpleLineDraw *_lineDrawObject;
    
}
@synthesize isTouchEnabled = _isTouchEnabled;
@synthesize levelObjective = _levelObjective;
@synthesize ballPointers =_ballPointers;




+(Level *)levelFromLevelNumber:(int)levelInt{
    
    LevelDataManager *levelManager = [LevelDataManager new];

    NSString *levelName = [levelManager getLevelNameForLevelNum:levelInt];
    NSString *levelPath = [NSString stringWithFormat:@"%@/%@", LEVELS_FOLDER, levelName];
    Level *levelToReturn = (Level*)[CCBReader load:levelPath];
    
    return levelToReturn;
}

-(void)setInitialLevelData{
    
    LevelDataManager *dataManager = [LevelDataManager new];
    
    _strokeRecord = [dataManager getStrokeRecordForLevelNum:[self getLevelNum]];
    // level is not new once it has loaded onto the screen
    [dataManager setIsNewStatus:FALSE forLevelNum:[self getLevelNum]];
    
    _objectiveComplete = FALSE;
    
    // init helper object
    _colorSwapObj = [StandardColorSwap new];
    _planeDivisionObj = [StandardPlaneDivision new];
    _lineDrawObject = [SimpleLineDraw new];
    
    _strokeLine = [CCDrawNode new];
    [self addChild:_strokeLine];
    
    [self resetTouchPoints];
    
}

- (void)didLoadFromCCB {
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    _objectiveComplete = FALSE;
    _numOfStrokes = 0;
    

}

-(BOOL)isLevelComplete{
    return _objectiveComplete;
}

-(BOOL)ballAnimationsDoneRunning{
    for (NSValue *ballPointer in self.ballPointers) {
        
        Ball *currentBall = (Ball*)[ballPointer pointerValue];
        
        if (currentBall.animationManager.runningSequenceName != nil) {
            return FALSE;
        }
    }
    
    return TRUE;
}

-(void)flagLevelIfCompleted{
    if ([_levelObjective isObjectiveComplete:_ballPointers] == TRUE) {
        _objectiveComplete = TRUE;
    }
}

-(int)getLevelNum{
    
    return _levelNum;
}

-(int)getStrokeRecord{
    return _strokeRecord;
}

-(int)getCurrentStrokes{
    
    return _numOfStrokes;
}


-(void)resetTouchPoints{
    _firstTouchPoint = ccp(0, 0);
    _secondTouchPoint = ccp(0, 0);
    _secondTouchPointSet = FALSE;
}

#pragma mark - Ball manipulation methods

//get all the ball pointers in the current level and push to NSArray of ballPointers
-(void)collectBallPointers{
    
    for (CCSprite *sprite in [self children]){
        if ([sprite isKindOfClass:[Ball class]]) {
            
            //create new ball to store place in memory, then create a pointer to that pointer
            //to add to self.ballpointers to manipulate balls later
            Ball *ballFromSprite = (Ball*)sprite;
            Ball *__strong *ballPointerPointer;
            ballPointerPointer = &ballFromSprite;
            
            NSValue *ballPPValue = [NSValue valueWithPointer:(__bridge const void *)(*ballPointerPointer)];
            
            [self.ballPointers addObject:ballPPValue];
            
        }
    }
}


-(void)partitionColorSwapBallsDefBy:(CGPoint)firstPoint andSecondPoint:(CGPoint)secondPoint{
    //divide balls into two planes with two arrays; return if a ball is on dividing line
    NSMutableArray *ballsOnFirstSide = [NSMutableArray array];
    NSMutableArray *ballsOnSecondSide = [NSMutableArray array];
    
    
    @try {
        for (NSValue *ballPointer in self.ballPointers) {
            Ball *currentBall = (Ball*)[ballPointer pointerValue];
            
            //push ball into array depending on which side of the line the ball is on
            if ([_planeDivisionObj point:currentBall.position
       relToLineDefByPoint1:firstPoint andPoint2:secondPoint] == kFirstSideOfLine) {
                [ballsOnFirstSide addObject:ballPointer];
            }
            else if ([_planeDivisionObj point:currentBall.position
            relToLineDefByPoint1:firstPoint andPoint2:secondPoint] == kSecondSideOfLine){
                [ballsOnSecondSide addObject:ballPointer];
            }
            else{
                CCLOG(@"A ball is ON the drawn line -[Gameplay touchEnded]");
                return;
            }
        }
        
    }
    @catch (NSException *exception) {
        CCLOG(@"[Gameplay TouchEnded] NSMutableArray not of Ball pointer values");
        return;
    }

    
    //swap balls if both sides of the plane are nonempty
    if ((int)[ballsOnFirstSide count] != 0 && (int)[ballsOnSecondSide count] != 0) {
        
        BallSwapBool *ballSwapBool = [BallSwapBool new];
        if ([ballSwapBool shouldSwapOccur:ballsOnFirstSide] || [ballSwapBool shouldSwapOccur:ballsOnSecondSide]) {
            
            
            [_colorSwapObj swapBalls:ballsOnFirstSide];
            [_colorSwapObj swapBalls:ballsOnSecondSide];
            
            // play sound here for swap success!
            OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
            [audio playEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, SWAP_AUDIO] volume:0.3 pitch:1.0 pan:0.0 loop:FALSE];
    
        }
        
        else{
            // play unsuccessful sound here!
            OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
            [audio playEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, NO_SWAP_AUDIO] volume:0.3 pitch:1.0 pan:0.0 loop:FALSE];
        }
        

    }
}



/* TOUCH METHODS: only process touch methods on level if level has not yet been completed */

#pragma mark - Touch Methods

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (_isTouchEnabled && [self isLevelComplete] == FALSE) {
        _firstTouchPoint = [touch locationInNode:self];
        
    }
}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (_isTouchEnabled && [self isLevelComplete] == FALSE) {
        _secondTouchPoint = [touch locationInNode:self];
        
        [_lineDrawObject setStrokeLineWithFirstPoint:_firstTouchPoint andSecondPoint:_secondTouchPoint inNode:self lineToDraw:_strokeLine];
        
        _secondTouchPointSet = TRUE;
    }
}

-(void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    if (_isTouchEnabled && [self isLevelComplete] == FALSE && _secondTouchPointSet == TRUE) {
        
        // only colorSwap if the two touch points are a certain distance from each other
        if (ccpDistance(_firstTouchPoint, _secondTouchPoint) > MIN_DIST_PT1_PT2) {
            
           CGPoint newFirstPoint =
                [self convertPositionFromPoints:_firstTouchPoint type:self.positionType];
           CGPoint newSecondPoint =
                [self convertPositionFromPoints:_secondTouchPoint type:self.positionType];
            
            
            
            [self partitionColorSwapBallsDefBy:newFirstPoint andSecondPoint:newSecondPoint];
            _numOfStrokes++; //increase stroke count
            [self flagLevelIfCompleted];
        }
        
        //reset touchpoints
        [self resetTouchPoints];

    }
}


@end
