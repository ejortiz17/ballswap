//
//  TutorialScene.m
//  BallSwap
//
//  Created by Eliud Ortiz on 2/18/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "TutorialScene.h"

@implementation TutorialScene{
    
    CCButton *_btnBack;
    CCButton *_btnNext;
    CCButton *_btnExit;
}

+(CCScene*)tutorialSceneWithCCBFileName:(NSString *)fileString{
    
    CCScene *tutorialScene = [CCBReader loadAsScene:@"TutorialScene"];
    
    // access the tutorialScene object from the CCScene
    TutorialScene *tutorialSceneObj = tutorialScene.children.firstObject;
    
    [tutorialSceneObj setTutorialSlideWithCCBFileName:@"fileString"];
    
    return tutorialScene;
    
}

+(CCScene*)tutorialSceneWithSlideNumber:(int)slideInt{
    
    CCScene *tutorialScene = [CCBReader loadAsScene:@"TutorialScene"];
    
    // access tutorial scene object from CCScene object
    TutorialScene *tutorialSceneObj = tutorialScene.children.firstObject;
    
    [tutorialSceneObj setTutorialSlideWithSlideNumber:slideInt];
    
    return tutorialScene;
}

-(void)didLoadFromCCB{
    // listen for swipes to the left
    UISwipeGestureRecognizer * swipeLeft= [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:swipeLeft];
    // listen for swipes to the right
    UISwipeGestureRecognizer * swipeRight= [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:swipeRight];
}


-(void)setTutorialSlideWithCCBFileName:(NSString *)fileString{
    
    // check if CCB file string is indeed a Tutorial Layer
    //
    // [fileString containsString:[NSString stringWithFormat:@"%@", TUTORIAL_LAYER_PREFIX]] <-- ios8 and yosemite
    if ([fileString rangeOfString:[NSString stringWithFormat:@"%@", TUTORIAL_LAYER_PREFIX]].location != NSNotFound) {
        
        NSString *fullFilePath = [NSString stringWithFormat:@"%@/%@",
                                  TUTORIAL_LAYER_FOLDER_PATH, fileString];
        
        _tutorialSlide = [CCBReader load:fullFilePath];
        
        if (_tutorialSlide != nil) {
            [self loadTutorialSlide];
        }
        else{
            CCLOG(@"Failed to set tutorial of file name %@", fullFilePath);
        }
    }
    
    else{
        CCLOG(@"\'%@\' is not a valid tutorial scene", fileString);
    }
}

-(void)setTutorialSlideWithSlideNumber:(int)slideInt{
    
    // append int value to the tutorial slide prefix
    NSString *fileString = [NSString stringWithFormat:@"%@%02d", TUTORIAL_LAYER_PREFIX, slideInt];
    
    [self setTutorialSlideWithCCBFileName:fileString];
}

// load tutorial layer in layerNode
-(void)loadTutorialSlide{
    CCScene *tutorialSlideScene = (CCScene*)_tutorialSlide;
    [_slideNode addChild: tutorialSlideScene];
}

-(CCNode*)getTutorialSlide{
    return _tutorialSlide;
}

-(int)getTutorialSlideInt{
    
    int _tutorialSlideInt;
    
    if (_tutorialSlide != nil) {
        
        NSString *stringInt = _tutorialSlide.name;
        _tutorialSlideInt = [stringInt intValue];
    }
    // negative value indicates nil _tutorialSlide
    else{
        _tutorialSlideInt = -1;
    }
    
    return _tutorialSlideInt;
}

// update back button
-(void)updateBackButtonState{
    
    // disable the back button if the tutorial slide is the 0th slide
    if ([self getTutorialSlideInt] == INIT_TUT_SLIDE_INT_VALUE) {
        [_btnBack setEnabled:FALSE];
    }
    else{
        if ([_btnBack enabled] == FALSE) {
            [_btnBack setEnabled:TRUE];
            
        }
        
    }
}

// update next button
-(void)updateNextButtonState{
    
    if ([self getTutorialSlideInt] == LAST_TUT_SLIDE_INT_VALUE) {
        [_btnNext setEnabled:FALSE];
    }
    else{
        if ([_btnNext enabled] == FALSE) {
            [_btnNext setEnabled:TRUE];
        }
    }
}


//touch methods
-(void)swipeLeft{
    [self toNextSlide];
}

-(void)swipeRight{
    [self toPreviousSlide];
}

-(void)toPreviousSlide{
    
    // get current slide value
    int currentSlideInt = [self getTutorialSlideInt];
    int nextSlideInt = currentSlideInt - 1;
    
    // if the next slide has a valid slide number within the range of values, then set it
    if (nextSlideInt >= INIT_TUT_SLIDE_INT_VALUE && nextSlideInt <= LAST_TUT_SLIDE_INT_VALUE) {
        CCScene* tutorialScene = [TutorialScene tutorialSceneWithSlideNumber:nextSlideInt];
        [[CCDirector sharedDirector]replaceScene:tutorialScene];
    }
    
    else{
        CCScene* tutorialScene = [TutorialScene tutorialSceneWithSlideNumber:INIT_TUT_SLIDE_INT_VALUE];
        [[CCDirector sharedDirector]replaceScene:tutorialScene];
    }
}

-(void)toNextSlide{
    // get current slide value
    int currentSlideInt = [self getTutorialSlideInt];
    int nextSlideInt = currentSlideInt + 1;
    
    // if the next slide has a valid slide number within the range of values, then set it to said slide
    if (nextSlideInt >= INIT_TUT_SLIDE_INT_VALUE && nextSlideInt <= LAST_TUT_SLIDE_INT_VALUE) {
        
        CCScene* tutorialScene = [TutorialScene tutorialSceneWithSlideNumber:nextSlideInt];
        [[CCDirector sharedDirector]replaceScene:tutorialScene];
    }
    
    else{
        CCScene* tutorialScene = [TutorialScene tutorialSceneWithSlideNumber:LAST_TUT_SLIDE_INT_VALUE];
        [[CCDirector sharedDirector]replaceScene:tutorialScene];
    }
    
}

-(void)toMainScene{
    
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]replaceScene:mainScene];
    
}

-(void)update:(CCTime)delta{
    
    [self updateNextButtonState];
    [self updateBackButtonState];
}


@end
