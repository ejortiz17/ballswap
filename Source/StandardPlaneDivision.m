//
//  StandardPlaneDivision.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/15/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "StandardPlaneDivision.h"

@implementation StandardPlaneDivision

//Creates a line from two points and tells what side of the plane that object is on
-(SideofLine)point:(CGPoint)point0 relToLineDefByPoint1:(CGPoint)point1 andPoint2:(CGPoint)point2{
    //calculate line from two points
    
    //try calculating the slope of the line
    @try {
        
        float slope = (point2.y - point1.y)/(point2.x - point1.x);
        
        //if slope is horizontal, firstSide is upper plane, secondSide is lower plane
        if (slope == 0) {
            //case where point 0 is on upper plane
            if (point0.y > point1.y) {
                return kFirstSideOfLine;
            }
            else if (point0.y < point1.y){
                return kSecondSideOfLine;
            }
            else{
                return kOnLine;
            }
        }
        
        //if slope is defined but not horizontal, the define the first side as the "greater than" side
        //of the line
        else if (slope < 0 || slope > 0){
            //to tell if point0 is above the line, find point on line with same x value
            //as point0, and compare the y values
            float yValToCompare = slope * (point0.x - point1.x) + point1.y;
            if (point0.y > yValToCompare) {
                return kFirstSideOfLine;
            }
            else if (point0.y < yValToCompare){
                return kSecondSideOfLine;
            }
            else{
                return kOnLine;
            }
        }
    }
    
    //handle the case where slope is undefined. We define the left side of the plane divided
    //by the line as firstSide and the right side of the plane as the second side
    @catch (NSException *exception) {
        if (point0.x < point1.x) {
            return kFirstSideOfLine;
        }
        else if (point0.x > point1.x){
            return kSecondSideOfLine;
        }
        else{
            return kOnLine;
        }
    }
    
}


@end
