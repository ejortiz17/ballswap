//
//  WinMenuLayer.h
//  BallSwap
//
//  Created by Eliud Ortiz on 1/17/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

@class Gameplay;
@class Level;
#import "CCNode.h"

@interface WinMenuLayer : CCNode{
    Level *_level;
}

-(void)setLevel:(Level*)newLevel;
-(void)setWinLabelString: (NSString*)newString;


-(void)toNextLevel;
-(void)replayLevel;
-(void)toLevelSelect;
-(void)toMainScene;


@end
