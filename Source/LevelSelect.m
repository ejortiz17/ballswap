//
//  LevelSelect.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/29/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "LevelSelect.h"

@implementation LevelSelect{
    
    CCButton *_btnMainMenu;
}

-(void)toMainScene{
    
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]replaceScene:mainScene];
    
}

@end
