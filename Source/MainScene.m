//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "Gameplay.h"
#import "GlobalStuff.h"

#import "TutorialScene.h"
#import "GameDataCacheManager.h"
#import "SessionDataManager.h"
#import "LevelDataManager.h"
#import "Constants.h"
#import "StandardGradientFade.h"

@implementation MainScene{
    
    CCButton *_btnPlay;
    CCNodeGradient *_bkgGradient;
}

- (void)didLoadFromCCB {
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    if ([[GameDataCacheManager sharedManager] isGameDataSet] == FALSE) {
        
        [[GameDataCacheManager sharedManager] initGameData];
    }
    
    if (![[GameDataCacheManager sharedManager]isKeyValid:NOTIFIED_GAME_COMPLETE]) {
        [[GameDataCacheManager sharedManager]setNotifiedUponGameCompletion:FALSE];
    }
    
   // BOOL someLevelPlayed = [[SessionDataManager]]
    // disable continue button if player has played no levels
    if (![[SessionDataManager sharedManager]someLevelPlayed]) {
    }
    
    // change play button to continue if game already started
    else if ([[SessionDataManager sharedManager] someLevelPlayed]){
        _btnPlay.label.string = @"Continue";
        [_btnPlay setTarget:self selector:@selector(continueGame)];
    }
    
}

-(void)startNewGame{
        
    if ([[GameDataCacheManager sharedManager] isGameDataSet] == FALSE) {
        [[GameDataCacheManager sharedManager] initGameData];
    }
    if ([[GameDataCacheManager sharedManager] isGameDataSet]) {
        
        CCScene *gamePlayScene = [Gameplay loadGameplayWithLevel:1];
        [[CCDirector sharedDirector] replaceScene:gamePlayScene];
    }
    else{
        CCLOG(@"Failed to set game data cache.");
    }
    
}

-(void)toLevelSelect{
    
    CCScene *levelSelectScene = [CCBReader loadAsScene:@"LevelSelect"];
    [[CCDirector sharedDirector] replaceScene:levelSelectScene];
}

-(void)toTutorial{
    
    CCScene *tutorialScene = [TutorialScene tutorialSceneWithSlideNumber:INIT_TUT_SLIDE_INT_VALUE];
    [[CCDirector sharedDirector] replaceScene:tutorialScene withTransition:
        [CCTransition transitionCrossFadeWithDuration:1.0]];
    
}

-(void)toStoreScene{
    
    CCScene *storeScene = [CCBReader loadAsScene:@"StoreScene"];
    [[CCDirector sharedDirector] replaceScene:storeScene];
    
}

-(void)continueGame{
   
    //int currentLevel = [[SessionDataManager sharedManager]getCurrentLevel];
    
    CCScene *gamePlayScene = [Gameplay loadGameplayWithLevel:[[SessionDataManager sharedManager]getCurrentLevel] ];
    [[CCDirector sharedDirector] replaceScene:gamePlayScene withTransition:[CCTransition transitionCrossFadeWithDuration:1.0]];
}

-(void)rotateBackGroundColor{
    
    // Generate random value between 0 and NUM_OF_COLORS - 1. Numbers to colors is bijective; pick color update color based on value
    int randVal = arc4random_uniform(NUM_OF_OBJECT_COLORS - 1);
    
    StandardGradientFade *fadeObject = [StandardGradientFade new];
    
    CCColor* oldColor = [_bkgGradient color];
    CCColor* newColor;
    
    switch (randVal) {
        case 0:
            newColor = [CCColor blueColor];
            break;
        case 1:
            newColor = [CCColor greenColor];
            break;
        case 2:
            newColor = [CCColor orangeColor];
            break;
        case 3:
            newColor = [CCColor redColor];
            break;
        case 4:
            newColor = [CCColor purpleColor];
            break;
            
        default:
            newColor = [CCColor grayColor];
            break;
    }
    
    if (![oldColor isEqualToColor:newColor]) {
        [fadeObject fadeGradient:_bkgGradient toColor:newColor];
    }
    
}

-(void)update:(CCTime)delta{
    
    if ([_bkgGradient numberOfRunningActions] == 0) {
        [self rotateBackGroundColor];
    }
}


@end
