//
//  SimpleLineDraw.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "SimpleLineDraw.h"

@implementation SimpleLineDraw

-(void)setStrokeLineWithFirstPoint:(CGPoint)point1 andSecondPoint:(CGPoint)point2 inNode:(CCNode *)nodeToDrawOn lineToDraw:(CCDrawNode *)strokeLine{
    
    if (strokeLine == nil) {
        CCLOG(@"Initialize strokeLine CCDrawNode before actually drawing");
    }
    else{
        
        [strokeLine clear];
        
        // draw line as red to show that the drawn line is invalid and won't register for being too short
        if (ccpDistance(point1, point2) <= MIN_DIST_PT1_PT2 ) {
            [strokeLine drawSegmentFrom:point1 to:point2 radius:1 color:[CCColor redColor]];
        }
        else{
            [strokeLine drawSegmentFrom:point1 to:point2 radius:1 color:[CCColor yellowColor]];
        }
        
    }
}

@end
