//
//  Gameplay.h
//  BallSwap
//
//  Created by Eliud Ortiz on 10/22/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//


#import "CCNode.h"
#import "CommonProtocols.h"
#import "Constants/Constants.h"
#import "Ball.h"
#import "Level.h"
#import "ObjectiveMenu.h"
#import "ContentNode.h"

@class WinMenuLayer;
@class PauseMenu;
@class SessionDataManager;


@interface Gameplay : CCNode

@property CGPoint firstPoint;
@property CGPoint secondPoint;

@property CCNode *objectiveMenuNode;
@property CCNode *winMenuNode;
@property CCNode *contentNode;
@property CCNode *levelNode;
//contains actual levelObject so one can access properties
@property Level *levelObject;
@property ObjectiveMenu *objectiveMenuObject;
@property WinMenuLayer *winMenuObject;

// button properties
@property CCButton *btnObjectiveMenu;
@property CCButton *btnPauseMenu;

// return a CCScene with a specific level into it
+(CCScene*)loadGameplayWithLevel:(int)levelInt;

// sets the level object and loads it into the levelNode
-(void)setAndLoadLevelObject:(Level*)levelToSet;

// set the objective menu with the levelObject property and generate the objective display
-(void)setAndLoadObjectiveMenuObj;

-(void)setAndLoadWinMenuObject;

-(void)pauseLevel;

//displays objective menu and disables touches for content node
-(void)displayObjectiveMenuNode;

// closes goal menu and enables touches on content node
-(void)hideObjectiveMenuNode;

// displays the Win Menu Node and disables content node touches
-(void)displayWinMenuNode;

// hides the Win Menu Node and disables touches in winMenu
-(void)hideWinMenuNode;




@end
