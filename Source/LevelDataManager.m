//
//  LevelDataManager.m
//  BallSwap
//
//  Created by Eliud Ortiz on 2/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "LevelDataManager.h"

#import "Level.h"

@implementation LevelDataManager


#pragma mark - Stored Data Creation
-(void)createLevelInCacheWithLevelNum:(int)levelNum{
   
    BOOL isLevelUnlocked = FALSE;
    
    // create a dictionary key for a level using its level number
    NSString* stringNumKey = [NSString stringWithFormat:@"%d",levelNum];
    
    //create Level Dictionary
    NSMutableDictionary *levelDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                            [NSString stringWithFormat:@"Level%d",levelNum] , LEVEL_NAME_KEY,
                                            [NSNumber numberWithInt:levelNum], LEVEL_NUM_KEY,
                                            [NSNumber numberWithInt:100], LEVEL_STROKE_RECORD_KEY,
                                            [NSNumber numberWithBool:isLevelUnlocked], LEVEL_IS_UNLOCKED_KEY,
                                            [NSNumber numberWithBool:FALSE], LEVEL_IS_COMPLETE_KEY,
                                            [NSNumber numberWithBool:TRUE], LEVEL_IS_NEW_KEY,
                                            [NSNumber numberWithBool:FALSE], LEVEL_IS_FREE_KEY,
                                            nil];
    
    // using the original string key from the plist file, store level dictionary in
    // NSUserDefaults.
    [[NSUserDefaults standardUserDefaults]setObject:levelDictionary forKey:stringNumKey];
    
}

-(void)createUnlockedLevelsListInCache{
    
    // create NSMutableArrays filled numbers of the levels that have been completed & unlocked
    NSMutableArray *unlockedLevels = [NSMutableArray array];
    
    // save unlocked levels and completed levels
    [[NSUserDefaults standardUserDefaults]
     setObject:unlockedLevels forKey:UNLOCKED_LEVELS_KEY];
     
}

-(void)createCompletedLevelsListInCache{
    
    // create NSMutableArrays filled numbers of the levels that have been completed & unlocked
    NSMutableArray *completedLevels = [NSMutableArray array];
    
    // save unlocked levels and completed levels
    [[NSUserDefaults standardUserDefaults]
     setObject:completedLevels forKey: COMPLETED_LEVELS_KEY];
    
}


#pragma mark - setters & getters
-(NSMutableDictionary*)getLevelDataForLevelNum:(int)levelNum{
    
    NSMutableDictionary *levelsDic = [NSMutableDictionary dictionary];
    // convert int to string to use for key
    NSString *intStringKey;
    // if the input levelInt is out of range, return dictionary initial level
    if (levelNum < INIT_LEVEL_NUM || levelNum> NUM_OF_LEVELS) {
        
        CCLOG(@"Level %i doesn't exist. Returning dictionary for inital level", levelNum);
        intStringKey = [NSString stringWithFormat:@"%i", INIT_LEVEL_NUM];
        levelsDic = [[NSUserDefaults standardUserDefaults]objectForKey:intStringKey];
    }
    
    else{
        
        intStringKey = [NSString stringWithFormat:@"%i", levelNum];
        levelsDic = [[NSUserDefaults standardUserDefaults] objectForKey:intStringKey];
    }
    
    return levelsDic;
}

-(void)setLevelData:(NSMutableDictionary *)levelData forLevelNum:(int)levelNum{
    
    //check if levelNum is valid
    if(levelNum < INIT_LEVEL_NUM || levelNum > NUM_OF_LEVELS){
        CCLOG(@"Level %i doesn't exist so level data could not be set", levelNum);
        return;
    }
    
    // keyArray contains a key that all valid levelData should have
    NSArray *keyArray = [NSArray arrayWithObjects:@"levelName", @"levelNumber",
                                                  @"strokeRecord", @"isUnlocked",
                                                  @"isComplete", @"isFree", nil];
    
    // check if levelData is valid and fits the format
    for (id obj in keyArray) {
        if ([levelData objectForKey:obj] == nil) {
            CCLOG(@"Level Data could not be set for level %i: No key \'%@\' exists.", levelNum, obj);
            return;
        }
    }
    
    NSString *intString = [NSString stringWithFormat:@"%d",levelNum];
    
    [[NSUserDefaults standardUserDefaults]setObject:levelData forKey:intString];
}

-(BOOL)doesLevelDataExistForLevelNum:(int)levelNum{
    
    NSMutableDictionary *levelsDic = [NSMutableDictionary dictionary];
    // convert int to string to use for key
    NSString *intStringKey = [NSString stringWithFormat:@"%i", levelNum];
    levelsDic = [[NSUserDefaults standardUserDefaults] objectForKey:intStringKey];
    
    if (levelsDic != nil) {
        return TRUE;
    }
    else{
        return FALSE;
    }
    
}

-(NSString*)getLevelNameForLevelNum:(int)levelNum{
    
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSString *levelName = [levelDic objectForKey:LEVEL_NAME_KEY];
    return levelName;
}

-(int)getStrokeRecordForLevelNum:(int)levelNum{
   
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSNumber *strokeRecord = [levelDic objectForKey:LEVEL_STROKE_RECORD_KEY];
    int strokeRecordToReturn = [strokeRecord intValue];
    
    return strokeRecordToReturn;
}

-(BOOL)getIsUnlockedStatusForLevelNum:(int)levelNum{
    
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSNumber *isUnlocked = [levelDic objectForKey:LEVEL_IS_UNLOCKED_KEY];
    
    return [isUnlocked boolValue];
}

-(BOOL)getIsCompleteStatusForLevelNum:(int)levelNum{
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSNumber *isComplete = [levelDic objectForKey:LEVEL_IS_COMPLETE_KEY];
    
    return [isComplete boolValue];
}

-(BOOL)getIsNewStatusForLevelNum:(int)levelNum{
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSNumber *isNew = [levelDic objectForKey:LEVEL_IS_NEW_KEY];
    
    return [isNew boolValue]; 
}

-(BOOL)getIsFreeStatusForLevelNum:(int)levelNum{
    NSMutableDictionary *levelDic = [self getLevelDataForLevelNum:levelNum];
    NSNumber *isFreeWrapper = [levelDic objectForKey:LEVEL_IS_FREE_KEY];
    
    return [isFreeWrapper boolValue];
}



-(void)setStrokeRecord:(int)strokeRecord forLevelNum:(int)levelNum{
    
    // create new dictionary with data of existing level dictionary
    NSMutableDictionary *levelDic = [NSMutableDictionary dictionaryWithDictionary:[self getLevelDataForLevelNum:levelNum]];
    
    // get current record from level dic
    int currentRecord = [[levelDic objectForKey:LEVEL_STROKE_RECORD_KEY] intValue];
    
    // if the new strokeRecord is less than currentRecord, rewrite the stroke record
    if (strokeRecord < currentRecord) {
        
        NSNumber *newStrokeRecord = [NSNumber numberWithInt:strokeRecord];
        [levelDic setObject:newStrokeRecord forKey:LEVEL_STROKE_RECORD_KEY];
        
        // overwrite stored data
        [self setLevelData:levelDic forLevelNum:levelNum];
    }
}

-(void)setIsUnlockedStatus:(BOOL)newUnlockedStatus forLevelNum:(int)levelNum{
    
    // create new dictionary with data of existing level dictionary
    NSMutableDictionary *levelDic = [NSMutableDictionary dictionaryWithDictionary:[self getLevelDataForLevelNum:levelNum]];
    [levelDic setValue:[NSNumber numberWithBool:newUnlockedStatus] forKey:LEVEL_IS_UNLOCKED_KEY];
    // overwrite stored data
    [self setLevelData:levelDic forLevelNum:levelNum];
    
    //add level to unlocked levels
    [self addLevelToUnlockedLevels:levelNum];
}

-(void)setIsCompleteStatus:(BOOL)newIsCompleteStatus forLevelNum:(int)levelNum{
    // create new dictionary with data of existing level dictionary
    NSMutableDictionary *levelDic = [NSMutableDictionary dictionaryWithDictionary:[self getLevelDataForLevelNum:levelNum]];
    [levelDic setValue:[NSNumber numberWithBool:newIsCompleteStatus] forKey:LEVEL_IS_COMPLETE_KEY];
    // overwrite stored data
    [self setLevelData:levelDic forLevelNum:levelNum];
    
    // add level to completed levels table
    [self addLevelToCompletedLevels:levelNum];
}

-(void)setIsNewStatus:(BOOL)isNewBool forLevelNum:(int)levelNum{
    
    // create new dictionary with data of existing level dictionary
    NSMutableDictionary *levelDic = [NSMutableDictionary dictionaryWithDictionary:[self getLevelDataForLevelNum:levelNum]];
    [levelDic setValue:[NSNumber numberWithBool:isNewBool] forKey:LEVEL_IS_NEW_KEY];
    // overwrite stored data
    [self setLevelData:levelDic forLevelNum:levelNum];
}

-(void)setIsFreeStatus:(BOOL)isFreeBool forLevelNum:(int)levelNum{
    // create new dictionary with data of existing level dictionary
    NSMutableDictionary *levelDic = [NSMutableDictionary dictionaryWithDictionary:[self getLevelDataForLevelNum:levelNum]];
    [levelDic setValue:[NSNumber numberWithBool:isFreeBool] forKey:LEVEL_IS_FREE_KEY];
    // overwrite stored data
    [self setLevelData:levelDic forLevelNum:levelNum];
}

-(NSMutableArray*)getUnlockedLevels{
    
    NSMutableArray *unlockedLevels =
    [[NSUserDefaults standardUserDefaults]objectForKey:UNLOCKED_LEVELS_KEY];
    
    return unlockedLevels;
}

-(void)setUnlockedLevels:(NSMutableArray *)unlockedLevels{
    [[NSUserDefaults standardUserDefaults] setObject:unlockedLevels forKey:UNLOCKED_LEVELS_KEY];
}

-(void)addLevelToUnlockedLevels:(int)levelNum{
    
    NSNumber *levelNumWrapper = [NSNumber numberWithInt:levelNum];
    NSMutableArray *replacementArray = [NSMutableArray arrayWithArray:[self getUnlockedLevels]];
    
    for (NSNumber* unlockedLevelNumWrapper in replacementArray) {
        if ([levelNumWrapper intValue] == [unlockedLevelNumWrapper intValue]) {
            CCLOG(@"Level %d already added to UnlockedLevels", [levelNumWrapper intValue]);
            return;
        }
    }
    [replacementArray addObject:levelNumWrapper];
    [self setUnlockedLevels:replacementArray];
}

-(NSMutableArray*)getCompletedLevels{
    
    NSMutableArray *completedLevels =
    [[NSUserDefaults standardUserDefaults] objectForKey:COMPLETED_LEVELS_KEY];
    
    return completedLevels;
    
}

-(void)setCompletedLevels:(NSMutableArray *)completedLevels{
    [[NSUserDefaults standardUserDefaults] setObject:completedLevels forKey:COMPLETED_LEVELS_KEY];
}

-(void)addLevelToCompletedLevels:(int)levelNum{
    
    NSNumber *levelNumWrapper = [NSNumber numberWithInt:levelNum];
    NSMutableArray *replacementArray = [NSMutableArray arrayWithArray:[self getCompletedLevels]];
    
    for (NSNumber* completedLevelNumWrapper in replacementArray) {
        if ([levelNumWrapper intValue] == [completedLevelNumWrapper intValue]) {
            CCLOG(@"Level %d already added to CompletedLevels", [levelNumWrapper intValue]);
            return;
        }
    }

    
    [replacementArray addObject:levelNumWrapper];
    [self setCompletedLevels:replacementArray];
}

#pragma mark - finder methods

-(int)getMaxUnlockedLevelNum{
    
    NSMutableArray *unlockedLevels = [self getUnlockedLevels];
    
    // check unlocked levelNumbers one by one and find the largest levelNum
    int maxUnlockedLevelNum = 0;
    for (NSNumber *levelNumWrapper in unlockedLevels) {
        
        if ([levelNumWrapper intValue] > maxUnlockedLevelNum) {
            
            maxUnlockedLevelNum = [levelNumWrapper intValue];
        }
    }
    
    return maxUnlockedLevelNum;
    
}

-(int)getNextLevelNumTo:(int)levelNum{
    
    int nextLevelNum = 1;
    
    @try {
        if ([self getIsUnlockedStatusForLevelNum:levelNum + 1]) {
            nextLevelNum = levelNum + 1;
        }
        
        else if (![self getIsUnlockedStatusForLevelNum:levelNum +1]){
            nextLevelNum = [self minUnlockedAndIncompleteLevel];
        }
        else{
            nextLevelNum = INIT_LEVEL_NUM;
        }
    }
    @catch (NSException *exception) {
        nextLevelNum = INIT_LEVEL_NUM;
    }
    @finally {
        return nextLevelNum;
    }
    
}

// get all locked levels
-(NSMutableArray*)getLockedLevels{
    
    NSMutableArray *lockedLevels = [NSMutableArray array];
    
    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS; j++) {
        
        // add levelNum to locked levels if indeed locked
        if (![self getIsUnlockedStatusForLevelNum:j]) {
            [lockedLevels addObject:[NSNumber numberWithInt:j]];
        }
    }
    
    return lockedLevels;
}

// get levels that are locked and free for the user
-(NSMutableArray*)getLockedFreeLevels{
    
    NSMutableArray *lockedFreeLevels = [NSMutableArray array];
    
    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS; j++) {
        
        // add levelNum to locked levels if indeed locked
        if (![self getIsUnlockedStatusForLevelNum:j] && [self getIsFreeStatusForLevelNum:j]) {
            [lockedFreeLevels addObject:[NSNumber numberWithInt:j]];
        }
    }
    return lockedFreeLevels;
}

// total number of levels have been complete by the player
-(int)getTotalCompletedLevels{

    int totalCompletedLevels = (int)[[self getCompletedLevels] count];
    
    return totalCompletedLevels;
}

-(int)getTotalCompletedFreeLevels{
    NSMutableArray *completedLevels = [self getCompletedLevels];
    
    int totalCompletedFree = 0;
    
    for (NSNumber* levelNumWrapper in completedLevels) {
        if ([self getIsFreeStatusForLevelNum:[levelNumWrapper intValue]]) {
            totalCompletedFree++;
        }
    }
    
    return totalCompletedFree;
}

// get total number of levels that have been unlocked by the player
-(int)getTotalUnlockedLevels{
    
    int totalUnlockedLevels = (int)[[self getUnlockedLevels] count];
    
    return totalUnlockedLevels;
}

-(int)getTotalUnlockedFreeLevels{
    NSMutableArray *unlockedLevels = [self getUnlockedLevels];
    
    int totalUnlockedFree = 0;
    
    for (NSNumber* levelNumWrapper in unlockedLevels) {
        if ([self getIsFreeStatusForLevelNum:[levelNumWrapper intValue]]) {
            totalUnlockedFree++;
        }
    }
    
    return totalUnlockedFree;
}


-(NSMutableArray*)intersectionUnlockedAndComplete{
    
    NSMutableArray *unlockedLevels = [self getUnlockedLevels];
    NSMutableArray *completedLevels= [self getCompletedLevels];
    
    NSMutableSet *unlockedLevelsSet = [NSMutableSet setWithArray:unlockedLevels];
    NSMutableSet *completedLevelsSet = [NSMutableSet setWithArray:completedLevels];
    
    // get intersection of unlocked and completed
    NSMutableSet *intersectionUnlockedAndCompleted = [NSMutableSet setWithSet:unlockedLevelsSet];
    [intersectionUnlockedAndCompleted intersectSet:completedLevelsSet];
    
    NSMutableArray *unlockedAndCompleted =
        [NSMutableArray arrayWithArray:[intersectionUnlockedAndCompleted allObjects]];
    
    return unlockedAndCompleted;
}

-(NSMutableArray*)intersectionUnlockedAndIncomplete{
    
    NSMutableArray *unlockedLevels = [self getUnlockedLevels];
    NSMutableArray *unlockedAndCompletedLevels = [self intersectionUnlockedAndComplete];
    
    NSMutableSet *unlockedAndIncomplete = [NSMutableSet setWithArray:unlockedLevels];
    
    //subtract unlocked and Completed form unlocked levels to get levels that are unlocked yet incomplete
    [unlockedAndIncomplete minusSet:[NSMutableSet setWithArray:unlockedAndCompletedLevels]];
    
    NSMutableArray *unlockedAndIncompleteArray = [NSMutableArray arrayWithArray:[unlockedAndIncomplete allObjects]];
    
    return unlockedAndIncompleteArray;
}

-(int)minUnlockedAndIncompleteLevel{
    
    NSMutableArray *unlockedAndIncompleteList = [self intersectionUnlockedAndIncomplete];
    // get min level number of the array
    NSNumber *minUnlockedAndIncompleteLvl = [unlockedAndIncompleteList valueForKeyPath:@"@min.self"];
    
    return [minUnlockedAndIncompleteLvl intValue];
    
}

#pragma mark - save methods
-(void)saveLevelDataforLevelNum:(int)levelNum strokeNum:(int)strokeNum isComplete:(BOOL)completeBool{
    
    [self setStrokeRecord:strokeNum forLevelNum:levelNum];
    
    if (completeBool == TRUE) {
        [self setIsCompleteStatus:TRUE forLevelNum:levelNum];
    }
}

#pragma mark - unlocking methods

// check if there remain levels to be unlocked
-(BOOL)levelsToUnlock{
    
    // are there levels to unlock left?
    BOOL levelsRemainingToUnlock = FALSE;
    
    // check each level to see if there are indeed levels to unlock
    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS; j++) {
       
        if ([self getIsUnlockedStatusForLevelNum:j]) {
            levelsRemainingToUnlock = TRUE;
            break;
        }
    }
    
    return levelsRemainingToUnlock;
}

-(BOOL)freeLevelsToUnlock{
    
    
    // are there levels to unlock left?
    BOOL levelsRemainingToUnlock = FALSE;
    
    // check each level to see if there are indeed levels to unlock
    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_FREE_LEVELS; j++) {
        
        if (![self getIsUnlockedStatusForLevelNum:j] && [self getIsFreeStatusForLevelNum:j]) {
            levelsRemainingToUnlock = TRUE;
            break;
        }
    }
    
    return levelsRemainingToUnlock;
    
}

-(void)unlockNextFreeLevels{
    
    // get locked free levels only
    NSMutableArray *lockedFreeLevelNums = [self getLockedFreeLevels];
    
    // do nothing if no free levels are locked
    if ((int)[lockedFreeLevelNums count]==0) {
        CCLOG(@"No free levels left to unlock!");
        return;
    }
    
    // sort lockedLevelNumbers from lowest to highest
    NSSortDescriptor *lowestToHighest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:TRUE];
    [lockedFreeLevelNums sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighest]];
    
    int numOfUnlockedLevels = [self getTotalUnlockedFreeLevels];
    int numOfCompletedLevels = [self getTotalCompletedFreeLevels];
    
    if (numOfUnlockedLevels - numOfCompletedLevels <= UNLOCKED_TO_COMPLETED_DIFFERENCE) {
        
        if ((int)[lockedFreeLevelNums count] > NUM_OF_LEVELS_TO_UNLOCK) {
            for (int j = 0; j < NUM_OF_LEVELS_TO_UNLOCK; j++) {
                
                NSNumber* levelNumWrapper = [lockedFreeLevelNums objectAtIndex:j];
                [self setIsUnlockedStatus:TRUE forLevelNum:[levelNumWrapper intValue]];
            }
        }
        
        else if((int)[lockedFreeLevelNums count] <= NUM_OF_LEVELS_TO_UNLOCK){
            
            for (NSNumber* levelNumWrapper in lockedFreeLevelNums) {
                [self setIsUnlockedStatus:TRUE forLevelNum:[levelNumWrapper intValue]];
            }
            
        }
        
    }
    
   /* else if((int)[lockedFreeLevelNums count] <= NUM_OF_LEVELS_TO_UNLOCK){
        
        for (NSNumber* levelNumWrapper in lockedFreeLevelNums) {
            [self setIsUnlockedStatus:TRUE forLevelNum:[levelNumWrapper intValue]];
        }
        
    } */
    
    
}

// num of levels to unlock and formula are determined by constants which can be changed
// returns true if levels are to be unlocked, false if they are not
-(void)unlockNextLevels{
    // get locked free levels only
    NSMutableArray *lockedLevelNums = [self getLockedLevels];
    
    // do nothing if no free levels are locked
    if ((int)[lockedLevelNums count]==0) {
        CCLOG(@"No levels left to unlock!");
        return;
    }
    
    // sort lockedLevelNumbers from lowest to highest
    NSSortDescriptor *lowestToHighest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:TRUE];
    [lockedLevelNums sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighest]];
    
    int numOfUnlockedLevels = [self getTotalUnlockedLevels];
    int numOfCompletedLevels = [self getTotalCompletedLevels];
    
    if (numOfUnlockedLevels - numOfCompletedLevels <= UNLOCKED_TO_COMPLETED_DIFFERENCE) {
        
        if ((int)[lockedLevelNums count] > NUM_OF_LEVELS_TO_UNLOCK) {
            for (int j = 0; j < NUM_OF_LEVELS_TO_UNLOCK; j++) {
                
                NSNumber* levelNumWrapper = [lockedLevelNums objectAtIndex:j];
                [self setIsUnlockedStatus:TRUE forLevelNum:[levelNumWrapper intValue]];
            }
        }
    }
    
    else if((int)[lockedLevelNums count] <= NUM_OF_LEVELS_TO_UNLOCK){
        
        for (NSNumber* levelNumWrapper in lockedLevelNums) {
            [self setIsUnlockedStatus:TRUE forLevelNum:[levelNumWrapper intValue]];
        }
        
    }
}

-(void)unlockNextLevelAfter:(int)levelNum{
    
    int nextLevelInt = levelNum + 1;
    
    [self setIsUnlockedStatus:TRUE forLevelNum:nextLevelInt];
}



@end
