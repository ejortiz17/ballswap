//
//  SessionDataManager.h
//  BallSwap
//
//  Created by Eliud Ortiz on 2/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
//  Singleton class meant to hold current user session information and initialize data

#import <Foundation/Foundation.h>
@class LevelDataManager;

@interface SessionDataManager : NSObject


+(SessionDataManager*)sharedManager;

-(void)createCurrentLevelSessionDataInCache;

-(void)setCurrentLevel:(int)newLevelNum;
-(void)setIsCurrentLevelCompleteStatus:(BOOL)newBool;

-(NSMutableDictionary*)getCurrentLevelSessionData;
-(int)getCurrentLevel;
-(BOOL)isCurrentLevelComplete;

// check if user has played at least some level
-(BOOL)someLevelPlayed;
@end
