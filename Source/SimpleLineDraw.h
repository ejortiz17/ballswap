//
//  SimpleLineDraw.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "DrawLine.h"

@interface SimpleLineDraw : NSObject <DrawLine>

@end
