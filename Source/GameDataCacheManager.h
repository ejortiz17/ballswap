//
//  GameDataCacheManager.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SessionDataManager;

@interface GameDataCacheManager : NSObject

+(GameDataCacheManager*)sharedManager;

-(BOOL)isKeyValid:(NSString*)stringKey;

-(void)initGameData;
-(void)deleteGameData;

// check if game data cache has been set
-(BOOL)isGameDataSet;


// check if all levels are to be unlocked
-(void)unlockAllLevelsAfterPurchase;
-(void)setUnlockAllLevels:(BOOL)newBool;
-(BOOL)unlockAllLevels;

-(void)setNotifiedUponGameCompletion:(BOOL)newBool;
-(BOOL)notifiedUponGameCompletion;


@end
