//
//  StandardPlaneDivision.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/15/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "PlaneDivisionAction.h"
#import <Foundation/Foundation.h>

@interface StandardPlaneDivision : NSObject <PlaneDivision>

@end
