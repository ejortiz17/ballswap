//
//  StandardColorSwap.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/10/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "StandardColorSwap.h"
#import "Constants.h"
#import "Ball.h"


@implementation StandardColorSwap

-(void)swapBalls:(NSMutableArray *)ballPointersArray{
    
    //count the colorCounter counts the total number of balls of a ball array color--
    //   @"BLUE"=>int, @"GREEN"=>int,
    //colorsToBeSwapped will contain all the colors that require swapping
    NSMutableDictionary *colorCounter = [NSMutableDictionary dictionary];
    NSMutableDictionary *colorsToBeSwapped = [NSMutableDictionary dictionary];
    
    @try {
        
        for (NSValue *ballPointerValue in ballPointersArray) {
            Ball *currentBall = (Ball*)[ballPointerValue pointerValue];
            NSString *currentBallColorString = [currentBall getColorInStringFormat];
            
            //if the current color hasn't been entered into colorCounter array, add the color string
            //as key with counter of 1
            if ([colorCounter objectForKey:currentBallColorString] == nil) {
                NSNumber *value = [NSNumber numberWithInt:1];
                [colorCounter setObject:value forKey:currentBallColorString];
            }
            //if the current color already exists, just add one to the counter in array
            else{
                NSNumber *value = [colorCounter objectForKey:currentBallColorString];
                int intValue = [value intValue];
                NSNumber *newValue = [NSNumber numberWithInt:intValue + 1];
                [colorCounter setObject:newValue forKey:currentBallColorString];
            }
            
        }
    }
    @catch (NSException *exception) {
        CCLOG(@"-[Level colorswap] exception: incompatible pointer");
        return;
    }
    
    //KEY IMPLEMENTATION OF SWAPPING ALGORITHM
    //Rule: If a multiple colors have the same amount of balls, exclude them from the list we will sort
    //e.g. RED=>3 , BLUE=>3, ORANGE=>3, SILVER=>1, VIOLET=>4; push only silver and violet
    //into the array colorsToBeSwapped
    
    for (NSString *currentColor0 in colorCounter) {
        
        //if flag is TRUE, we will add currentColor0 to colorsToBeSwapped (NSMutableDic)
        BOOL currentColor0ValueUnique = TRUE;
        
        for (NSString *currentColor1 in colorCounter ) {
            if ([currentColor1 isEqualToString:currentColor0]) {
                continue;
            }
            
            NSNumber *currentColor0Val = [colorCounter objectForKey:currentColor0];
            NSNumber *currentColor1Val = [colorCounter objectForKey:currentColor1];
            //break out of inner loop if the two colors being compared have
            //the same amount of colored balls
            if ([currentColor1Val intValue] == [currentColor0Val intValue]) {
                currentColor0ValueUnique = FALSE;
                break;
            }
        }
        
        if (currentColor0ValueUnique == TRUE){
            [colorsToBeSwapped setObject:[colorCounter objectForKey:currentColor0]
                                  forKey:currentColor0];
        }
    }
    
    //sort color strings based on value of NSNumber from lowest to highest
    NSArray *sortedColors = [colorsToBeSwapped keysSortedByValueUsingComparator:
                             ^NSComparisonResult(id obj1, id obj2) {
                                 
                                 if ([obj1 intValue] > [obj2 intValue]) {
                                     return (NSComparisonResult)NSOrderedDescending;
                                 }
                                 else if ([obj1 intValue] < [obj2 intValue]) {
                                     return NSOrderedAscending;
                                 }
                                 else {
                                     return NSOrderedSame;
                                 }
                             }];
    
    //swap colors based if the array is of length even or odd;
    //e.g. {[0],[1],[4]} vs {[3],[5],[6],[7]}: in the odd array, the middle element is unchanged
    // in the rest, the last is paired with the first, second to last with second, etc.
    
    //make a copy of the ballPointers for quicker processing and removal
    NSMutableArray *ballPointersArrayCopy = [NSMutableArray arrayWithArray:ballPointersArray];
    NSMutableArray *ballPointersToRemove = [NSMutableArray array];
    
    if (sortedColors != nil) {
        
        //swap colors for even-length sortedColors to be swapped
        if ((int)[sortedColors count] % 2 == 0) {
            for (int j = 0; j < (int)[sortedColors count]/2; j++) {
                @try {
                    
                    for (NSValue *ballPointerValue in ballPointersArrayCopy) {
                        Ball *currentBall = (Ball*)[ballPointerValue pointerValue];
                        NSString *sortedColorsAtIndexJ = [sortedColors objectAtIndex:j];
                        NSString *sortedColorsAtIndexJComplement = [sortedColors objectAtIndex:([sortedColors count]-1)-j];
                        
                        //swap color of current ball with its pair:
                        //sortedColors[j] <=> sortedColors[(count-1)-j]
                        if ([[currentBall getColorInStringFormat] isEqualToString:sortedColorsAtIndexJ]) {
                            [currentBall setColorWithAnimation:sortedColorsAtIndexJComplement];
                            //[currentBall blueFadeOutAction];
                            [ballPointersToRemove addObject:ballPointerValue];
                        }
                        
                        else if ([[currentBall getColorInStringFormat] isEqualToString:sortedColorsAtIndexJComplement]){
                            [currentBall setColorWithAnimation:sortedColorsAtIndexJ];
                            //[currentBall blueFadeOutAction];
                            [ballPointersToRemove addObject:ballPointerValue];
                        }
                    }
                    [ballPointersArrayCopy removeObjectsInArray:ballPointersToRemove];
                }
                @catch (NSException *exception) {
                    //add code to throw for exception
                    CCLOG(@"Incorrect Pointer type -[Level colorSwap]");
                    return;
                }
            }
        }
        
        //swap colors for odd-length sortedColors to be swapped
        else if ((int)[sortedColors count] % 2 == 1){
            for (int j = 0; j < ((int)[sortedColors count] - 1)/2; j++) {
                //
                @try {
                    
                    //loop through every ballPointerValue to attempt to swap colors
                    for(NSValue *ballPointerValue in ballPointersArrayCopy){
                        Ball *currentBall = (Ball*)[ballPointerValue pointerValue];
                        NSString *sortedColorsAtIndexJ = [sortedColors objectAtIndex:j];
                        NSString *sortedColorsAtIndexJComplement = [sortedColors objectAtIndex:([sortedColors count]-1)-j];
                        
                        //swap color of current ball with its pair:
                        //sortedColors[j] <=> sortedColors[(count-1)-j]
                        if ([[currentBall getColorInStringFormat] isEqualToString:sortedColorsAtIndexJ]) {
                            [currentBall setColorWithAnimation:sortedColorsAtIndexJComplement];
                            [ballPointersToRemove addObject:ballPointerValue];
                        }
                        
                        else if ([[currentBall getColorInStringFormat] isEqualToString:sortedColorsAtIndexJComplement]){
                            [currentBall setColorWithAnimation:sortedColorsAtIndexJ];
                            //[currentBall blueFadeOutAction];
                            [ballPointersToRemove addObject:ballPointerValue];
                        }
                    }
                    [ballPointersArrayCopy removeObjectsInArray:ballPointersToRemove];
                }
                @catch (NSException *exception) {
                    CCLOG(@"Incorrect Pointer type -[Level colorSwap]");
                    return;
                    
                }
                
            }
            
        }
        
        
    }
    
    
}


@end
