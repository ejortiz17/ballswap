//
//  PauseMenu.m
//  BallSwap
//
//  Created by Eliud Ortiz on 1/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "PauseMenu.h"

#import "Gameplay.h"

@implementation PauseMenu

+(CCScene*)loadPauseMenuSceneWithLevelNum:(int)levelInt{
    
    CCScene *pauseMenuScene = [CCBReader loadAsScene:@"PauseMenu"];
    
    // access actual PauseMenu object from scene and use it to set the int value
    
    PauseMenu *pauseMenuObj = (PauseMenu*)pauseMenuScene.children.firstObject;
    
    // set up PauseMenu
    [pauseMenuObj setLevelNum:levelInt];
    
    return pauseMenuScene;
    
}

-(void)setLevelNum:(int)newLevelInt{
    
    if (INIT_LEVEL_NUM <= newLevelInt && newLevelInt <= NUM_OF_LEVELS) {
        _levelNum = newLevelInt;
    }
    
    else{
        CCLOG(@"[PauseMenu setLevelNum]: \'%i\' is an invalid level number", newLevelInt);
        CCLOG(@"initializing with initial level instead");
        _levelNum = INIT_LEVEL_NUM;
    }
}

-(void)resumeLevel{
    
    // pop scene off of CCDirector stack
    [[CCDirector sharedDirector] popScene];
}

-(void)replayLevel{
    
    @try {
        
        CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:_levelNum];
        [[CCDirector sharedDirector] replaceScene:newGameplayScene];
    }
    
    // if _currentLevel is nil, load initial level
    @catch (NSException *exception){
        
        CCScene *newGameplayScene = [Gameplay loadGameplayWithLevel:INIT_LEVEL_NUM];
        CCLOG(@"WinMenuLayer _currentLevel is nil. Loading first level instead");
        [[CCDirector sharedDirector] replaceScene:newGameplayScene];
    }
}

-(void)toLevelSelect{
    
    CCScene *levelSelectScene = [CCBReader loadAsScene:LEVEL_SELECT_MENU];
    [[CCDirector sharedDirector]replaceScene:levelSelectScene];
}

-(void)toMainScene{
    
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]replaceScene:mainScene];
    
}


@end
