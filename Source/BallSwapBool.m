//
//  BallSwapBool.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "BallSwapBool.h"
#import "Constants.h"
#import "Ball.h"
#import "GlobalStuff.h"

@implementation BallSwapBool

-(BOOL)shouldSwapOccur:(NSMutableArray *)ballPointers{
   
    NSMutableDictionary *colorCounter = [NSMutableDictionary dictionary];
    NSMutableDictionary *colorsToBeSwapped = [NSMutableDictionary dictionary];

    
    for (NSValue *ballPointerValue in ballPointers) {
        Ball *currentBall = (Ball*)[ballPointerValue pointerValue];
        NSString *currentBallColorString = [currentBall getColorInStringFormat];
        
        //if the current color hasn't been entered into colorCounter array, add the color string
        //as key with counter of 1
        if ([colorCounter objectForKey:currentBallColorString] == nil) {
            NSNumber *value = [NSNumber numberWithInt:1];
            [colorCounter setObject:value forKey:currentBallColorString];
        }
        //if the current color already exists, just add one to the counter in array
        else{
            NSNumber *value = [colorCounter objectForKey:currentBallColorString];
            int intValue = [value intValue];
            NSNumber *newValue = [NSNumber numberWithInt:intValue + 1];
            [colorCounter setObject:newValue forKey:currentBallColorString];
        }
        
    }
    
    for (NSString *currentColor0 in colorCounter) {
        
        //if flag is TRUE, we will add currentColor0 to colorsToBeSwapped (NSMutableDic)
        BOOL currentColor0ValueUnique = TRUE;
        
        for (NSString *currentColor1 in colorCounter ) {
            if ([currentColor1 isEqualToString:currentColor0]) {
                continue;
            }
            
            NSNumber *currentColor0Val = [colorCounter objectForKey:currentColor0];
            NSNumber *currentColor1Val = [colorCounter objectForKey:currentColor1];
            //break out of inner loop if the two colors being compared have
            //the same amount of colored balls
            if ([currentColor1Val intValue] == [currentColor0Val intValue]) {
                currentColor0ValueUnique = FALSE;
                break;
            }
        }
        
        if (currentColor0ValueUnique == TRUE){
            [colorsToBeSwapped setObject:[colorCounter objectForKey:currentColor0]
                                  forKey:currentColor0];
        }
    }
    
    if ((int)[colorsToBeSwapped count] >= 2) {
        return TRUE;
    }
    else{
        return FALSE;
    }
}

@end
