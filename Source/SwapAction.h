//
//  SwapAction.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/10/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#ifndef BallSwap_SwapAction_h
#define BallSwap_SwapAction_h

@protocol BallSwap
-(void)swapBalls:(NSMutableArray*)ballPointersArray;
@end

@protocol BallSwapBool <NSObject>

-(BOOL)shouldSwapOccur:(NSMutableArray*)ballPointers;

@end


#endif
