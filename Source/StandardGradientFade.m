//
//  StandardGradientFade.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "StandardGradientFade.h"

@implementation StandardGradientFade 

-(void)fadeGradient:(CCNodeGradient *)gradient toColor:(CCColor *)newColor{
    
    CCActionTintTo *tintAction = [CCActionTintTo actionWithDuration:2.0 color:newColor];
    [gradient runAction:tintAction];
}

@end
