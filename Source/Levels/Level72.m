//
//  Level72.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/11/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level72.h"
#import "ColorNumObjective.h"

@implementation Level72{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 72;
    ///////////////////////////
    
    [self setInitialLevelData];
    
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorOrange];
    [_ball2 setColorAndFrame:kColorOrange];
    [_ball3 setColorAndFrame:kColorOrange];
    
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorBlue];
    
    [_ball6 setColorAndFrame:kColorBlue];
    [_ball7 setColorAndFrame:kColorBlue];
    
    [_ball8 setColorAndFrame:kColorViolet];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:1], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], ORANGE_COLOR_STRING,
                                          [NSNumber numberWithInt:6], VIOLET_COLOR_STRING,
                                          nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end

