//
//  Level62.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/11/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level62.h"
#import "ColorNumObjective.h"

@implementation Level62{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
    Ball *_ball11;
    Ball *_ball12;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 62;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorViolet];
    [_ball2 setColorAndFrame:kColorViolet];
    [_ball3 setColorAndFrame:kColorViolet];
    [_ball4 setColorAndFrame:kColorViolet];
    
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorGreen];
    
    [_ball7 setColorAndFrame:kColorRed];
    [_ball8 setColorAndFrame:kColorGreen];
    
    [_ball9 setColorAndFrame:kColorRed];
    [_ball10 setColorAndFrame:kColorBlue];
    [_ball11 setColorAndFrame:kColorBlue];
    [_ball12 setColorAndFrame:kColorBlue];
    
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:2], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:3], GREEN_COLOR_STRING,

                                          [NSNumber numberWithInt:4], RED_COLOR_STRING,
                                          [NSNumber numberWithInt:3], VIOLET_COLOR_STRING, nil];
    
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end
