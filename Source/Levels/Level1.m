//
//  Level1.m
//  BallSwap
//
//  Created by Eliud Ortiz on 2/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level1.h"

@implementation Level1{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
}


- (void)didLoadFromCCB {
    
        
    _levelNum = 1;
    
    
    [self setInitialLevelData];

    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:2], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], RED_COLOR_STRING, nil];
    
    self.ballPointers = [NSMutableArray array];
    
    [_ball1 setColorAndFrame:kColorRed];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];

    [self collectBallPointers];
    
    [self setPositionType:CCPositionTypeNormalized];
    
}

@end
