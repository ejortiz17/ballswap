//
//  Level29.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/18/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level29.h"
#import "ColorNumObjective.h"

@implementation Level29{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;

}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 29;
    ///////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorOrange];
    [_ball4 setColorAndFrame:kColorGreen];
    [_ball5 setColorAndFrame:kColorOrange];

    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:1], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], ORANGE_COLOR_STRING, nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end
