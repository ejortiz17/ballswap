//
//  Level41.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/24/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level41.h"
#import "ColorNumObjective.h"

@implementation Level41{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 41;
    ///////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorViolet];
    [_ball2 setColorAndFrame:kColorViolet];
    [_ball3 setColorAndFrame:kColorOrange];
    [_ball4 setColorAndFrame:kColorViolet];
    [_ball5 setColorAndFrame:kColorOrange];
    [_ball6 setColorAndFrame:kColorOrange];
    [_ball7 setColorAndFrame:kColorViolet];
    [_ball8 setColorAndFrame:kColorOrange];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:7], ORANGE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], VIOLET_COLOR_STRING, nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end
