//
//  Level28.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/18/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level28.h"
#import "ColorNumObjective.h"

@implementation Level28{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 28;
    ///////////////////////////
    
    [self setInitialLevelData];

    
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorGreen];
    [_ball4 setColorAndFrame:kColorOrange];
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorBlue];
    [_ball7 setColorAndFrame:kColorBlue];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:3], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], ORANGE_COLOR_STRING, nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}

@end
