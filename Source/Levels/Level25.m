//
//  Level25.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level25.h"
#import "ColorNumObjective.h"

@implementation Level25{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 25;
    ///////////////////////////
    
    [self setInitialLevelData];

    
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorRed];
    [_ball4 setColorAndFrame:kColorRed];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorGreen];
    [_ball7 setColorAndFrame:kColorGreen];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:1], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:5], RED_COLOR_STRING, nil];
    

    
    [self setPositionType:CCPositionTypeNormalized];
    
}



@end
