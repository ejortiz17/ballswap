//
//  Level52.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/10/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level52.h"
#import "PosColorObjective.h"

@implementation Level52{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 52;
    ///////////////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    NSValue *_ball7Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball7)];
    NSValue *_ball8Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball8)];
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorBlue];
    [_ball6 setColorAndFrame:kColorOrange];
    [_ball7 setColorAndFrame:kColorOrange];
    [_ball8 setColorAndFrame:kColorOrange];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           ORANGE_COLOR_STRING, _ball1Pointer,
                                           BLUE_COLOR_STRING, _ball2Pointer,
                                           BLUE_COLOR_STRING, _ball3Pointer,
                                           BLUE_COLOR_STRING, _ball4Pointer,
                                           BLUE_COLOR_STRING, _ball5Pointer,
                                           BLUE_COLOR_STRING, _ball6Pointer,
                                           BLUE_COLOR_STRING, _ball7Pointer,
                                           BLUE_COLOR_STRING, _ball8Pointer, nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}


@end
