//
//  Level74.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/12/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level74.h"
#import "ColorNumObjective.h"

@implementation Level74{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 74;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorOrange];
    [_ball2 setColorAndFrame:kColorOrange];
    [_ball3 setColorAndFrame:kColorOrange];
    [_ball4 setColorAndFrame:kColorBlue];
    
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorBlue];
    
    [_ball7 setColorAndFrame:kColorGreen];
    [_ball8 setColorAndFrame:kColorViolet];
    [_ball9 setColorAndFrame:kColorViolet];
    [_ball10 setColorAndFrame:kColorViolet];
    
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:7], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:1], ORANGE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], VIOLET_COLOR_STRING,
                                          nil];
    
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end
