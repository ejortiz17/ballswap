//
//  Level77.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/12/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level77.h"

#import "ColorNumObjective.h"

@implementation Level77{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
    Ball *_ball11;
    Ball *_ball12;
    Ball *_ball13;
    Ball *_ball14;
    Ball *_ball15;
    
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 77;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorGreen];
    
    [_ball4 setColorAndFrame:kColorOrange];
    [_ball5 setColorAndFrame:kColorOrange];
    [_ball6 setColorAndFrame:kColorOrange];
    
    [_ball7 setColorAndFrame:kColorViolet];
    [_ball8 setColorAndFrame:kColorViolet];
    [_ball9 setColorAndFrame:kColorViolet];
    [_ball10 setColorAndFrame:kColorViolet];
    
    [_ball11 setColorAndFrame:kColorRed];
    [_ball12 setColorAndFrame:kColorRed];
    [_ball13 setColorAndFrame:kColorRed];
    [_ball14 setColorAndFrame:kColorRed];
    [_ball15 setColorAndFrame:kColorRed];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:3], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:3], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], ORANGE_COLOR_STRING,
                                          [NSNumber numberWithInt:3], RED_COLOR_STRING,
                                          [NSNumber numberWithInt:4], VIOLET_COLOR_STRING,
                                          nil];
    
    
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end

