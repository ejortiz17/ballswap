//
//  Level20.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level20.h"
#import "ColorNumObjective.h"

@implementation Level20{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
}

- (void)didLoadFromCCB {
    
    _levelNum = 20;
    
    
    [self setInitialLevelData];


    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:1], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:3], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], RED_COLOR_STRING, nil];
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorRed];
    
    [self collectBallPointers];
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end
