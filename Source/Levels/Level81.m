//
//  Level81.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/13/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level81.h"
#import "ColorNumObjective.h"

@implementation Level81{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 81;
    ///////////////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorGreen];
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorOrange];
    
    [_ball6 setColorAndFrame:kColorBlue];
    [_ball7 setColorAndFrame:kColorBlue];
    [_ball8 setColorAndFrame:kColorGreen];
    [_ball9 setColorAndFrame:kColorGreen];
    
    [_ball10 setColorAndFrame:kColorBlue];
    
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:6], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], ORANGE_COLOR_STRING,
                                          nil];
    
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end

