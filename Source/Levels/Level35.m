//
//  Level35.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/21/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level35.h"
#import "PosColorObjective.h"

@implementation Level35{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 35;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    NSValue *_ball7Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball7)];
    NSValue *_ball8Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball8)];
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorRed];
    [_ball2 setColorAndFrame:kColorRed];
    [_ball3 setColorAndFrame:kColorRed];
    [_ball4 setColorAndFrame:kColorViolet];
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorOrange];
    [_ball7 setColorAndFrame:kColorOrange];
    [_ball8 setColorAndFrame:kColorOrange];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           GREEN_COLOR_STRING, _ball1Pointer,
                                           GREEN_COLOR_STRING, _ball2Pointer,
                                           GREEN_COLOR_STRING, _ball3Pointer,
                                           VIOLET_COLOR_STRING, _ball4Pointer,
                                           ORANGE_COLOR_STRING, _ball5Pointer,
                                           RED_COLOR_STRING, _ball6Pointer,
                                           RED_COLOR_STRING, _ball7Pointer,
                                           RED_COLOR_STRING, _ball8Pointer, nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end

