//
//  Level22.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level22.h"
#import "PosColorObjective.h"

@implementation Level22{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    
}

- (void)didLoadFromCCB {
    
    _levelNum = 22;
    
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           BLUE_COLOR_STRING, _ball1Pointer,
                                           GREEN_COLOR_STRING, _ball2Pointer,
                                           RED_COLOR_STRING, _ball3Pointer,
                                           GREEN_COLOR_STRING, _ball4Pointer, nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    self.ballPointers = [NSMutableArray array];
    
    // set ball colors
    [_ball1 setColorAndFrame:kColorRed];
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorGreen];
    
    [self collectBallPointers];
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end
