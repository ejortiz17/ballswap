//
//  Level26.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/17/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level26.h"
#import "PosColorObjective.h"

@implementation Level26{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 26;
    ///////////////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    NSValue *_ball7Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball7)];
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorRed];
    [_ball4 setColorAndFrame:kColorRed];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorGreen];
    [_ball7 setColorAndFrame:kColorGreen];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           GREEN_COLOR_STRING, _ball1Pointer,
                                           GREEN_COLOR_STRING, _ball2Pointer,
                                           RED_COLOR_STRING, _ball3Pointer,
                                           RED_COLOR_STRING, _ball4Pointer,
                                           RED_COLOR_STRING, _ball5Pointer,
                                           BLUE_COLOR_STRING, _ball6Pointer,
                                           BLUE_COLOR_STRING, _ball7Pointer, nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}
@end
