//
//  Level63.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/11/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level63.h"
#import "ColorNumObjective.h"

@implementation Level63{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
    Ball *_ball11;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 63;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    
    [_ball3 setColorAndFrame:kColorGreen];
    [_ball4 setColorAndFrame:kColorGreen];
    [_ball5 setColorAndFrame:kColorGreen];
    
    [_ball6 setColorAndFrame:kColorOrange];
    
    [_ball7 setColorAndFrame:kColorGreen];
    [_ball8 setColorAndFrame:kColorGreen];
    [_ball9 setColorAndFrame:kColorGreen];
    
    [_ball10 setColorAndFrame:kColorBlue];
    [_ball11 setColorAndFrame:kColorBlue];
    
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:4], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:4], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:3], ORANGE_COLOR_STRING,
                                           nil];
    
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end
