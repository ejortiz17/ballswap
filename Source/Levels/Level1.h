//
//  Level1.h
//  BallSwap
//
//  Created by Eliud Ortiz on 2/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level.h"
#import "Ball.h"
#import "ColorNumObjective.h"
#import "Constants.h"

@interface Level1 : Level

@end
