//
//  Level38.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/23/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level38.h"
#import "ColorNumObjective.h"

@implementation Level38{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 38;
    ///////////////////////////
    
    [self setInitialLevelData];
    
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorGreen];
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorRed];
    [_ball7 setColorAndFrame:kColorRed];
    [_ball8 setColorAndFrame:kColorRed];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:2], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:5], RED_COLOR_STRING, nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end
