//
//  Level50.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/26/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level50.h"
#import "PosColorObjective.h"

@implementation Level50{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
    Ball *_ball11;
    Ball *_ball12;
    Ball *_ball13;
    Ball *_ball14;
    Ball *_ball15;
    Ball *_ball16;
    
    
    
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 50;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    NSValue *_ball7Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball7)];
    NSValue *_ball8Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball8)];
    NSValue *_ball9Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball9)];
    NSValue *_ball10Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball10)];
    NSValue *_ball11Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball11)];
    NSValue *_ball12Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball12)];
    NSValue *_ball13Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball13)];
    NSValue *_ball14Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball14)];
    NSValue *_ball15Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball15)];
    NSValue *_ball16Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball16)];

    
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorGreen];
    [_ball2 setColorAndFrame:kColorRed];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorViolet];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorBlue];
    [_ball7 setColorAndFrame:kColorViolet];
    [_ball8 setColorAndFrame:kColorBlue];
    [_ball9 setColorAndFrame:kColorBlue];
    [_ball10 setColorAndFrame:kColorViolet];
    [_ball11 setColorAndFrame:kColorBlue];
    [_ball12 setColorAndFrame:kColorRed];
    [_ball13 setColorAndFrame:kColorViolet];
    [_ball14 setColorAndFrame:kColorBlue];
    [_ball15 setColorAndFrame:kColorRed];
    [_ball16 setColorAndFrame:kColorGreen];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           VIOLET_COLOR_STRING, _ball1Pointer,
                                           BLUE_COLOR_STRING, _ball2Pointer,
                                           BLUE_COLOR_STRING, _ball3Pointer,
                                           BLUE_COLOR_STRING, _ball4Pointer,
                                           
                                           BLUE_COLOR_STRING, _ball5Pointer,
                                           BLUE_COLOR_STRING, _ball6Pointer,
                                           BLUE_COLOR_STRING, _ball7Pointer,
                                           RED_COLOR_STRING, _ball8Pointer,
                                           
                                           BLUE_COLOR_STRING, _ball9Pointer,
                                           BLUE_COLOR_STRING, _ball10Pointer,
                                           RED_COLOR_STRING, _ball11Pointer,
                                           RED_COLOR_STRING, _ball12Pointer,
                                           
                                           BLUE_COLOR_STRING, _ball13Pointer,
                                           RED_COLOR_STRING, _ball14Pointer,
                                           RED_COLOR_STRING, _ball15Pointer,
                                           GREEN_COLOR_STRING, _ball16Pointer,
                                           nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end

