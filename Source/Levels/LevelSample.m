//
//  LevelSample.m
//  BallSwap
//
//  Created by Eliud Ortiz on 11/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelSample.h"

@implementation LevelSample{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
}


- (void)didLoadFromCCB {
    
    _levelNum = 2;
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:3], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], RED_COLOR_STRING,
                                          [NSNumber numberWithInt:1], GREEN_COLOR_STRING, nil];
    
    self.ballPointers = [NSMutableArray array];
    
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorRed];
    [_ball4 setColorAndFrame:kColorRed];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorGreen];
    
    [self collectBallPointers];
    
    CCLOG(@"Level Number: %i", [self getLevelNum]);
    
}

@end
