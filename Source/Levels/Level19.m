//
//  Level19.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level19.h"
#import "PosColorObjective.h"

@implementation Level19{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
}

- (void)didLoadFromCCB {
    
    _levelNum = 19;
    
    
    
    [self setInitialLevelData];

    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           RED_COLOR_STRING, _ball1Pointer,
                                           GREEN_COLOR_STRING, _ball2Pointer,
                                           BLUE_COLOR_STRING, _ball3Pointer,
                                           GREEN_COLOR_STRING, _ball4Pointer,
                                           RED_COLOR_STRING, _ball5Pointer,
                                           BLUE_COLOR_STRING, _ball6Pointer, nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    self.ballPointers = [NSMutableArray array];
    
    [_ball1 setColorAndFrame:kColorRed];
    [_ball2 setColorAndFrame:kColorRed];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorGreen];
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorBlue];
    
    [self collectBallPointers];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}


@end
