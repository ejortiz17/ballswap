//
//  Level79.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/12/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level79.h"
#import "ColorNumObjective.h"

@implementation Level79{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 79;
    ///////////////////////////////////
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorOrange];
    
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorGreen];
    [_ball4 setColorAndFrame:kColorGreen];
    [_ball5 setColorAndFrame:kColorGreen];
    
    [_ball6 setColorAndFrame:kColorViolet];
    [_ball7 setColorAndFrame:kColorViolet];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:4], ORANGE_COLOR_STRING,
                                          [NSNumber numberWithInt:1], VIOLET_COLOR_STRING,
                                          nil];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}
@end
