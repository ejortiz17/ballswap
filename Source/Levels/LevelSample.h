//
//  LevelSample.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/15/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Level.h"
#import "Ball.h"
#import "ColorNumObjective.h"
#import "Constants.h"

@interface LevelSample : Level{
}

@end
