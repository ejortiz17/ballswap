//
//  Level18.m
//  BallSwap
//
//  Created by Eliud Ortiz on 3/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level18.h"
#import "ColorNumObjective.h"

@implementation Level18{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
}

- (void)didLoadFromCCB {
    
    _levelNum = 18;
    
    
    
    [self setInitialLevelData];

    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:2], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:2], RED_COLOR_STRING, nil];
    
    self.ballPointers = [NSMutableArray array];
    
    [_ball1 setColorAndFrame:kColorBlue];
    [_ball2 setColorAndFrame:kColorRed];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorGreen];
    [_ball6 setColorAndFrame:kColorBlue];
    
    [self collectBallPointers];
    
    [self setPositionType:CCPositionTypeNormalized];
    
}

@end
