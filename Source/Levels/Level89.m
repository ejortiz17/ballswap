//
//  Level89.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/13/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level89.h"
#import "ColorNumObjective.h"

@implementation Level89{
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////
    _levelNum = 89;
    ///////////////////////////
    
    [self setInitialLevelData];

    
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    
    self.ballPointers = [NSMutableArray array];
    
    // set initial ball colors
    [_ball1 setColorAndFrame:kColorGreen];
    [_ball2 setColorAndFrame:kColorGreen];
    [_ball3 setColorAndFrame:kColorGreen];
    
    [_ball4 setColorAndFrame:kColorRed];
    [_ball5 setColorAndFrame:kColorRed];
    [_ball6 setColorAndFrame:kColorRed];
    [_ball7 setColorAndFrame:kColorRed];
    
    [_ball8 setColorAndFrame:kColorBlue];
    
    [self collectBallPointers];
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [ColorNumObjective new];
    self.levelObjective.goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithInt:3], BLUE_COLOR_STRING,
                                          [NSNumber numberWithInt:2], GREEN_COLOR_STRING,
                                          [NSNumber numberWithInt:3], RED_COLOR_STRING,
                                          nil];
    
    
    
    [self setPositionType:CCPositionTypeNormalized];
    
}


@end


