//
//  Level82.m
//  BallSwap
//
//  Created by Eliud Ortiz on 4/13/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Level82.h"
#import "PosColorObjective.h"

@implementation Level82{
    
    Ball *_ball1;
    Ball *_ball2;
    Ball *_ball3;
    Ball *_ball4;
    Ball *_ball5;
    Ball *_ball6;
    Ball *_ball7;
    Ball *_ball8;
    Ball *_ball9;
    Ball *_ball10;
    Ball *_ball11;
}

- (void)didLoadFromCCB {
    
    ///////////////////////////////////
    _levelNum = 82;
    ///////////////////////////////////
    
    [self setInitialLevelData];
    
    
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    NSValue *_ball1Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball1)];
    NSValue *_ball2Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball2)];
    NSValue *_ball3Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball3)];
    NSValue *_ball4Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball4)];
    NSValue *_ball5Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball5)];
    NSValue *_ball6Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball6)];
    NSValue *_ball7Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball7)];
    NSValue *_ball8Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball8)];
    NSValue *_ball9Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball9)];
    NSValue *_ball10Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball10)];
    NSValue *_ball11Pointer = [NSValue valueWithPointer:(__bridge const void *)(_ball11)];

    self.ballPointers = [NSMutableArray array];
    
    // set all the ball colors
    [_ball1 setColorAndFrame:kColorOrange];
    [_ball2 setColorAndFrame:kColorBlue];
    [_ball3 setColorAndFrame:kColorBlue];
    [_ball4 setColorAndFrame:kColorBlue];
    [_ball5 setColorAndFrame:kColorOrange];
    
    [_ball6 setColorAndFrame:kColorGreen];
    [_ball7 setColorAndFrame:kColorGreen];
    [_ball8 setColorAndFrame:kColorOrange];
    
    [_ball9 setColorAndFrame:kColorViolet];
    [_ball10 setColorAndFrame:kColorViolet];
    [_ball11 setColorAndFrame:kColorViolet];
    
    [self collectBallPointers];
    
    
    //create the levelObjective and check if the objective is complete
    self.levelObjective = [PosColorObjective new];
    NSMutableDictionary *goalDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           VIOLET_COLOR_STRING, _ball1Pointer,
                                           BLUE_COLOR_STRING, _ball2Pointer,
                                           BLUE_COLOR_STRING, _ball3Pointer,
                                           BLUE_COLOR_STRING, _ball4Pointer,
                                           BLUE_COLOR_STRING, _ball5Pointer,
                                           
                                           GREEN_COLOR_STRING, _ball6Pointer,
                                           GREEN_COLOR_STRING, _ball7Pointer,
                                           BLUE_COLOR_STRING, _ball8Pointer,
                                           ORANGE_COLOR_STRING, _ball9Pointer,
                                           ORANGE_COLOR_STRING, _ball10Pointer,
                                           ORANGE_COLOR_STRING, _ball11Pointer,
                                           nil];
    
    [self.levelObjective initGoalDictionary:goalDictionary];
    
    // make everything positioned in the node relative in percentages
    [self setPositionType:CCPositionTypeNormalized];
}

@end


