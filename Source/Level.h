//
//  Level.h
//  BallSwap
//
//  Created by Eliud Ortiz on 11/4/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "LevelObjective.h"
#import "Ball.h"
#import "CommonProtocols.h"

@class  LevelDataManager;
@class BallSwapBool;

#ifndef LEVEL_H
#define LEVEL_H
@interface Level : CCNode {
    
    int _levelNum;
    int _numOfStrokes;
    int _strokeRecord;
    CCDrawNode *_strokeLine;
}

// if isToucheEnabled == T, then allow BallSwapping
@property BOOL isTouchEnabled;

@property LevelObjective *levelObjective;
@property NSMutableArray *ballPointers; //must be initialized in CCBLoad function


+(Level*)levelFromLevelNumber:(int)levelInt;

// sets level data like stroke record on level setup
-(void)setInitialLevelData;

-(void)collectBallPointers;
-(void)partitionColorSwapBallsDefBy:(CGPoint)firstPoint andSecondPoint:(CGPoint)secondPoint;

//checks if ball animations are finished playing
-(BOOL)ballAnimationsDoneRunning;

// checks if level objective is completed
-(BOOL)isLevelComplete;

//if level is completed flag it as completed
-(void)flagLevelIfCompleted;

-(int)getLevelNum;

-(int)getStrokeRecord;

-(int)getCurrentStrokes;




@end

#endif