//
//  MainScene.h
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "Gameplay.h"

@class TutorialScene;
@class LevelDataManager;
@class SessionDataManager;
@class StandardGradientFade;


@interface MainScene : CCNode{
}

-(void)continueGame;
-(void)startNewGame;
-(void)toLevelSelect;
-(void)toStoreScene;
-(void)toTutorial;
@end
