//
//  Gameplay.m
//  BallSwap
//
//  Created by Eliud Ortiz on 10/22/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Gameplay.h"

#import "WinMenuLayer.h"
#import "PauseMenu.h"
#import "SessionDataManager.h"
#import "LevelDataManager.h"
#import "GameDataCacheManager.h"

@implementation Gameplay{
    
    CCLabelTTF *_lblCurrentScore;
    CCLabelTTF *_lblHiScore;
    CCLabelTTF *_lblLevelNum;
}

@synthesize firstPoint = _firstPoint;
@synthesize secondPoint = _secondPoint;

@synthesize objectiveMenuNode = _objectiveMenuNode;
@synthesize winMenuNode = _winMenuNode;
@synthesize contentNode = _contentNode;
@synthesize levelNode = _levelNode;
@synthesize levelObject = _levelObject;
@synthesize objectiveMenuObject = _objectiveMenuObject;
@synthesize winMenuObject = _winMenuObject;

@synthesize btnObjectiveMenu = _btnObjectiveMenu;
@synthesize btnPauseMenu = _btnPauseMenu;

+(CCScene*)loadGameplayWithLevel:(int)levelInt{
    
    // update sessiondatamanager to current level
    [[SessionDataManager sharedManager]setCurrentLevel:levelInt];
    
    Level *levelToLoad = [Level levelFromLevelNumber:levelInt];
    
    CCScene *gameplayScene = [CCBReader loadAsScene:@"Gameplay"];
    
    //access actual Gameplay object from scene to set level
    Gameplay *gameplayObj = (Gameplay*)gameplayScene.children.firstObject;
    
    // set and load the level and the objective level menu for gameplay
    [gameplayObj setAndLoadLevelObject:levelToLoad];
    [gameplayObj setAndLoadObjectiveMenuObj];
    [gameplayObj setAndLoadWinMenuObject];
    [gameplayObj setLevelNumLabel];
    

    
    
    return gameplayScene;
}

// is called when CCB file has completed loading
- (void)didLoadFromCCB {
    // tell this scene to accept touches
    self.userInteractionEnabled = TRUE;
    
    [self hideWinMenuNode];
    [self displayObjectiveMenuNode];

    

    
}
-(void) touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
}

-(void) touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
}

-(void) touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    
}

-(void)touchCancelled:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    
    
}

-(void)setAndLoadLevelObject:(Level*)levelToSet{
    _levelObject = levelToSet;
    CCScene *levelLayer = (CCScene*)_levelObject;
    [_levelNode addChild:levelLayer];
    
}

-(void)setAndLoadObjectiveMenuObj{
    if (_levelObject != nil) {
        
        [_objectiveMenuObject setLevel:_levelObject];
        [_objectiveMenuObject generateObjectiveDisplay];
    }
    
    else{
        CCLOG(@"[Gameplay setAndLoadObjectiveMenuObj] could not load _objective b/c _levelObject is nil");
    }
}

-(void)setAndLoadWinMenuObject{
    
    if (_levelObject != nil) {
        [_winMenuObject setLevel:_levelObject];
    }
    
    else{
        CCLOG(@"[Gameplay setAndLoadWinMenuObject] could not set winMenuLayer because _levelObject is nil");
    }
}

-(void)pauseLevel{
    
    CCScene *pauseMenuScene = [PauseMenu loadPauseMenuSceneWithLevelNum:[_levelObject getLevelNum]];
    [[CCDirector sharedDirector] pushScene:pauseMenuScene];
    
}

-(void)setLevelNumLabel{
    
    // if current level is set, display label to current level's number
    if (_levelObject != nil) {
        _lblLevelNum.string = [NSString stringWithFormat:@"%d", [_levelObject getLevelNum]];
    }
    
}

// display goal Menu and disable the contentNode from receiving touches to prevent other
// buttons from being pressed
-(void)displayObjectiveMenuNode{
    _objectiveMenuNode.visible = TRUE;
    _objectiveMenuNode.userInteractionEnabled = TRUE;
    _levelObject.isTouchEnabled = FALSE;
    _btnObjectiveMenu.enabled = FALSE;
    _btnPauseMenu.enabled = FALSE;
}

-(void)hideObjectiveMenuNode{
    _objectiveMenuNode.visible = FALSE;
    _objectiveMenuNode.userInteractionEnabled = FALSE;
    _levelObject.isTouchEnabled = TRUE;
    _btnObjectiveMenu.enabled = TRUE;
    _btnPauseMenu.enabled = TRUE;

}

-(void)displayWinMenuNode{
    
    //set _winMenuNode position to be measured in percentage of container
    [_winMenuNode setPositionType:CCPositionTypeNormalized];
    
    // have _winMenuNode drop in for animation
    CCActionMoveTo *moveAction = [CCActionMoveTo actionWithDuration:.5 position:ccp(0, 0)];
    
    [_winMenuNode runAction:moveAction];
    
    _winMenuNode.visible = TRUE;
    _winMenuNode.userInteractionEnabled = TRUE;
    _levelObject.isTouchEnabled = FALSE;
    
    // disable buttons and make them disappear
    _btnObjectiveMenu.enabled = FALSE;
    _btnPauseMenu.enabled = FALSE;
    _btnObjectiveMenu.visible = FALSE;
    _btnPauseMenu.visible = FALSE;
    
}

-(void)hideWinMenuNode{
    _winMenuNode.visible = FALSE;
    _winMenuNode.userInteractionEnabled = FALSE;
    _levelObject.isTouchEnabled = TRUE;
}

-(void)updateCurrentScoreLabel{
    
    int currentScore = [self.levelObject getCurrentStrokes];
    
    _lblCurrentScore.string = [NSString stringWithFormat:@"Score: %d", currentScore];
    
}

-(void)displayHighScoreLabel{
    
    if (self.levelObject != nil) {
        // get high score for current level
        int highScore = [self.levelObject getStrokeRecord];
        
        _lblHiScore.string = [NSString stringWithFormat:@"Record: %d", highScore];
    }

}

-(void)update:(CCTime)delta{
    
    [self updateCurrentScoreLabel];
    [self displayHighScoreLabel];
    
    // display _winMenuNode if level is complete and the _winMenuNode isn't already visible
    if ([self.levelObject isLevelComplete] == TRUE && _winMenuNode.visible == FALSE &&
                                           [self.levelObject ballAnimationsDoneRunning] == TRUE){
        LevelDataManager *levelManager = [LevelDataManager new];
        // update current session data
        [[SessionDataManager sharedManager]setCurrentLevel:[levelManager getNextLevelNumTo:[_levelObject getLevelNum]]];
        /*[[SessionDataManager sharedManager]
         setIsCurrentLevelCompleteStatus:[_levelObject isLevelComplete]];*/
        
        
        // save level data
        [levelManager saveLevelDataforLevelNum:[_levelObject getLevelNum] strokeNum:[_levelObject getCurrentStrokes] isComplete:[_levelObject isLevelComplete]];
        
        
        // unlock new levels only if the difference between unlocked and completed is const int
        // UNLOCKED_TO_COMPLETED
        
        if ([levelManager freeLevelsToUnlock] == TRUE &&
            ([levelManager getTotalUnlockedFreeLevels] - [levelManager getTotalCompletedFreeLevels]) <= UNLOCKED_TO_COMPLETED_DIFFERENCE) {
            
            [levelManager unlockNextFreeLevels];
            [_winMenuObject setWinLabelString:@"New Levels Unlocked!"];
        }
        
        if (![[GameDataCacheManager sharedManager]unlockAllLevels] && [levelManager getTotalCompletedFreeLevels] == NUM_OF_FREE_LEVELS) {
            [_winMenuObject setWinLabelString:@"Upgrade to unlock \nALL Levels!"];
        }
        
        if ([levelManager getTotalCompletedLevels] == NUM_OF_LEVELS && ![[GameDataCacheManager sharedManager] notifiedUponGameCompletion]) {
            [_winMenuObject setWinLabelString:@"ALL LEVELS COMPLETE!"];
            [[GameDataCacheManager sharedManager] setNotifiedUponGameCompletion:TRUE];
        }
        
        // play success sound
        OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
        [audio playEffect:[NSString stringWithFormat:@"%@/%@", AUDIO_FOLDER, SUCCESS_AUDIO] volume:0.3 pitch:1.0 pan:0.0 loop:FALSE];

        [self displayWinMenuNode];
        CCLOG(@"YOU WIN!");
    }

}







@end

