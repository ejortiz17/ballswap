//
//  PlaneDivisionAction.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/16/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
#import "CommonProtocols.h"

#ifndef BallSwap_PlaneDivisionAction_h
#define BallSwap_PlaneDivisionAction_h

@protocol PlaneDivision <NSObject>

-(SideofLine)point:(CGPoint)point0 relToLineDefByPoint1:(CGPoint)point1 andPoint2:(CGPoint)point2;

@end

#endif
