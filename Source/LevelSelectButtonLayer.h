//
//  LevelSelectButtonLayer.h
//  BallSwap
//
//  Created by Eliud Ortiz on 1/19/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@class Gameplay;
@class Level;

@interface LevelSelectButtonLayer : CCNode{
}

// change background images of buttons depending on which levels are locked and which aren't
-(void)setButtonSprites;

// collect all CCButtons in self
-(NSMutableArray*)getButtonsArray;

// go to level of a clicked button
-(void)goToButtonLevel:(CCButton*)sender;
@end
