//
//  SessionDataManager.m
//  BallSwap
//
//  Created by Eliud Ortiz on 2/25/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
// Singleton class meant to manage current session data

#import "SessionDataManager.h"
#import "Constants.h"
#import "LevelDataManager.h"

@implementation SessionDataManager{

}

+(SessionDataManager*)sharedManager{

    static SessionDataManager *_sharedManager = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[SessionDataManager alloc] init];
    });
    
    return _sharedManager;
}

-(void)createCurrentLevelSessionDataInCache{
    
    NSMutableDictionary *currentLevelSessionArray = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], CURRENT_LEVEL_KEY,
        [NSNumber numberWithBool:FALSE], IS_CURRENT_LEVEL_COMPLETE_KEY,
                                                                nil];
    
    [[NSUserDefaults standardUserDefaults]setObject:currentLevelSessionArray forKey:SESSION_DATA_KEY];
}

#pragma mark - setter methods
-(void)setCurrentSessionData:(NSMutableDictionary*)newLevelSessionData{
    
    [[NSUserDefaults standardUserDefaults]setObject:newLevelSessionData forKey:SESSION_DATA_KEY];
}

-(void)setCurrentLevel:(int)newLevelNum{
    
    NSMutableDictionary *newSessionDic =
        [NSMutableDictionary dictionaryWithDictionary:[self getCurrentLevelSessionData]];
    
    // check for setting new levelNum
    LevelDataManager *levelManager = [LevelDataManager new];
    if ([levelManager getIsUnlockedStatusForLevelNum:newLevelNum]) {
        newLevelNum = newLevelNum;
    }
    
    // if the attemoted level to set is not unlocked, find minUnlockedAndIncompleteLevel
    else if (![levelManager getIsUnlockedStatusForLevelNum:newLevelNum]){
        newLevelNum = [levelManager minUnlockedAndIncompleteLevel];
    }
    
    else{
        newLevelNum = INIT_LEVEL_NUM;
    }
    [newSessionDic setObject:[NSNumber numberWithInt:newLevelNum] forKey:CURRENT_LEVEL_KEY];
    [self setCurrentSessionData:newSessionDic];
    
}

-(void)setIsCurrentLevelCompleteStatus:(BOOL)newBool{
    
    NSMutableDictionary *newSessionDic =
        [NSMutableDictionary dictionaryWithDictionary:[self getCurrentLevelSessionData]];
    [newSessionDic setObject:[NSNumber numberWithBool:newBool] forKey:IS_CURRENT_LEVEL_COMPLETE_KEY];
    [self setCurrentSessionData:newSessionDic];
    
}

#pragma mark - getter methods
-(NSMutableDictionary*)getCurrentLevelSessionData{
    
    NSMutableDictionary *currentLevelSessionData =
        [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults]objectForKey:SESSION_DATA_KEY]];
   return currentLevelSessionData;
}

-(int)getCurrentLevel{
    
    NSMutableDictionary *currentSessionDic = [NSMutableDictionary dictionaryWithDictionary:[self getCurrentLevelSessionData]];
    int currentLevelNum = [[currentSessionDic objectForKey:CURRENT_LEVEL_KEY]intValue];
    return currentLevelNum;
}

-(BOOL)isCurrentLevelComplete{
    
    NSMutableDictionary *currentSessionDic = [self getCurrentLevelSessionData];
    BOOL isCurrentLevelComplete =
        [[currentSessionDic objectForKey:IS_CURRENT_LEVEL_COMPLETE_KEY]boolValue];
    return isCurrentLevelComplete;
}

-(BOOL)someLevelPlayed{
    
    BOOL someLevelPlayed = FALSE;
    
    LevelDataManager *levelManager = [LevelDataManager new];
    
    // loop through each level's data. If a level was played, return true
    for (int j = INIT_LEVEL_NUM; j <= NUM_OF_LEVELS; j++) {
        if (![levelManager getIsNewStatusForLevelNum:j]) {
            someLevelPlayed = TRUE;
            break;
        }
    }
    
    return someLevelPlayed;
}




@end
