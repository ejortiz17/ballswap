//
//  TutorialScene.h
//  BallSwap
//
//  Created by Eliud Ortiz on 2/18/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//


#import "CCNode.h"
#import "Constants.h"

@interface TutorialScene : CCNode{
    
    // layer containing Tutorial Slide
    CCNode *_slideNode;
    
    CCNode *_tutorialSlide;
}

+(CCScene*)tutorialSceneWithCCBFileName:(NSString*)fileString;

// returns a tutorial as a scene with an initial slide 
+(CCScene*)tutorialSceneWithSlideNumber:(int)slideInt;

-(void)setTutorialSlideWithCCBFileName:(NSString*)fileString;
-(void)setTutorialSlideWithSlideNumber:(int)slideInt;
-(CCNode*)getTutorialSlide;

// get the int value of the current tutorial layer slide
-(int)getTutorialSlideInt;

// sends the tutorial scene to the next slide in tutorial
-(void)toNextSlide;
// sends tutorial scene to previous slide in tutorial
-(void)toPreviousSlide;
-(void)toMainScene;

-(void)swipeLeft;
-(void)swipeRight;


@end
