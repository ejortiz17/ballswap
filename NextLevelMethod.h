//
//  NextLevelMethod.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/22/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
#import "Constants.h"
#ifndef BallSwap_NextLevelMethod_h
#define BallSwap_NextLevelMethod_h

@protocol NextLevelNum <NSObject>

-(int)getNextLevelNum;

@end

#endif
