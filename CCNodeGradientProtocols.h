//
//  CCNodeGradientProtocols.h
//  BallSwap
//
//  Created by Eliud Ortiz on 4/27/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CommonProtocols.h"
#ifndef BallSwap_CCNodeGradientProtocols_h
#define BallSwap_CCNodeGradientProtocols_h

@protocol ColorChange <NSObject>

-(void)fadeGradient:(CCNodeGradient*)gradient toColor:(CCColor*)newColor;

@end

#endif
